@RailDesktop @RailDesktopSearchTicketFeature @UI
Feature: Rail Desktop Search Ticket Feature

  Background: [DESKTOP] user open rail search page
    Given [desktop-home-page] user open home page blibli
    And [desktop-home-page] user close iframe if show up
    And [desktop-home-page] user decline location permission
    And [desktop-home-page] user click all category
    And [desktop-favourites-page] user click kereta
    Then [desktop-rail-search-page] user should see rail search form

  @Positive
  Scenario: [DESKTOP] user see list popular city
    When [desktop-rail-search-page] user click departure location
    Then [desktop-rail-search-page] user should see list popular city
    When [desktop-rail-search-page] user click destination location
    Then [desktop-rail-search-page] user should see list popular city

  @Positive
  Scenario: [DESKTOP] user search ticket with valid data
    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city 'validDeptCity'
    And [desktop-rail-search-page] user fill departure station 'validDeptStation'
    And [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city 'validDestCity'
    And [desktop-rail-search-page] user fill destination station 'validDestStation'
    And [desktop-rail-search-page] user fill date of departure with day plus 4
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

  @Negative
  Scenario: [DESKTOP] user search ticket with invalid city or station
    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city 'invalidDeptCity'
    Then [desktop-rail-search-page] user should see search error message

    When [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city 'invalidDestCity'
    Then [desktop-rail-search-page] user should see search error message

  @Negative
  Scenario Outline: [DESKTOP] user search ticket with empty station : departure station <deptStation>, destination station <destStation>
    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city '<deptCity>'
    And [desktop-rail-search-page] user fill departure station '<deptStation>'
    And [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city '<destCity>'
    And [desktop-rail-search-page] user fill destination station '<destStation>'
    And [desktop-rail-search-page] user fill date of departure with day plus 4
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-search-page] user should see empty station error message '<emptyStationErrorMessage>'
    Examples:
      | deptCity      | deptStation      | destCity      | destStation      | emptyStationErrorMessage                 |
      | validDeptCity | validDeptStation |               |                  | Silakan Pilih Stasiun Tujuan Anda        |
      |               |                  | validDestCity | validDestStation | Silakan Pilih Stasiun Keberangkatan Anda |

  @Negative
  Scenario Outline: [DESKTOP] user search ticket with invalid total passenger : total adult passenger <totalAdultPassenger>, total infant passenger <totalInfantPassenger>
    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city 'validDeptCity'
    And [desktop-rail-search-page] user fill departure station 'validDeptStation'
    And [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city 'validDestCity'
    And [desktop-rail-search-page] user fill destination station 'validDestStation'
    And [desktop-rail-search-page] user fill date of departure with day plus 4
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger   | totalInfantPassenger   |
      | <totalAdultPassenger> | <totalInfantPassenger> |
    Then [desktop-rail-search-page] user should see total passenger error message '<totalPassengerErrorMessage>'
    Examples:
      | totalAdultPassenger | totalInfantPassenger | totalPassengerErrorMessage                     |
      | 0                   | 0                    | Jumlah minimal orang dewasa: 1 orang           |
      | 1                   | 2                    | Jumlah bayi tidak boleh melebihi jumlah dewasa |
      | 5                   | 0                    | Jumlah maksimal orang dewasa: 4 orang          |
      | 4                   | 5                    | Jumlah maksimal bayi: 4 bayi                   |