@RailDesktop @RailDesktopListTicketFeature @UI
Feature: Rail Desktop List Ticket Feature

  Background: [DESKTOP] user fill date departure
    Given [desktop-home-page] user open home page blibli
    And [desktop-home-page] user close iframe if show up
    And [desktop-home-page] user decline location permission
    And [desktop-home-page] user click all category
    And [desktop-favourites-page] user click kereta
    Then [desktop-rail-search-page] user should see rail search form

    When [desktop-rail-search-page] user click departure location
    And [desktop-rail-search-page] user fill departure city 'validDeptCity'
    And [desktop-rail-search-page] user fill departure station 'validDeptStation'
    And [desktop-rail-search-page] user click destination location
    And [desktop-rail-search-page] user fill destination city 'validDestCity'
    And [desktop-rail-search-page] user fill destination station 'validDestStation'
    And [desktop-rail-search-page] user fill date of departure with day plus 4

  @Positive
  Scenario: [DESKTOP] user filter by time
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user filter departure time
    Then [desktop-rail-list-page] user should see filtered list departure time

  @Positive
  Scenario: [DESKTOP] user filter by class
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user filter departure class
    Then [desktop-rail-list-page] user should see filtered list departure class

  @Positive
  Scenario Outline: [DESKTOP] user filter by price with filter price <filterPrice>
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user filter departure price to '<filterPrice>' price
    Then [desktop-rail-list-page] user should see filtered list '<filterPrice>' departure price
    Examples:
      | filterPrice |
      | MAX         |
      | MIN         |

  @Positive
  Scenario: [DESKTOP] user filter by train type
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user filter departure train type
    Then [desktop-rail-list-page] user should see filtered list departure train type

  @Positive
  Scenario: [DESKTOP] user change ticket
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user click ganti pencarian
    And [desktop-rail-list-page] user click departure location
    And [desktop-rail-list-page] user change departure city 'validChangeDeptCity'
    And [desktop-rail-list-page] user change departure station 'validChangeDeptStation'
    And [desktop-rail-list-page] user click destination location
    And [desktop-rail-list-page] user change destination city 'validChangeDestCity'
    And [desktop-rail-list-page] user change destination station 'validChangeDestStation'
    And [desktop-rail-list-page] user change date of departure with day plus 5
    And [desktop-rail-list-page] user change passenger
      | changeTotalAdultPassenger      | changeTotalInfantPassenger      |
      | validChangeTotalAdultPassenger | validChangeTotalInfantPassenger |
    And [desktop-rail-list-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

  @Positive
  Scenario: [DESKTOP] user choose ticket departure (one way)
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user click choose departure ticket
    Then [desktop-rail-checkout-page] user should see order detail
    And [desktop-rail-checkout-page] user should see origin station
    And [desktop-rail-checkout-page] user should see destination station
    And [desktop-rail-checkout-page] user should see ticket price
    And [desktop-rail-checkout-page] user should see total ticket price
    And [desktop-rail-checkout-page] user should see total passenger
    And [desktop-rail-checkout-page] user should see date

  @Positive
  Scenario: [DESKTOP] user choose ticket departure and return (round trip)
    And [desktop-rail-search-page] user fill date of return with day plus 5
    And [desktop-rail-search-page] user fill passenger
      | totalAdultPassenger      | totalInfantPassenger      |
      | validTotalAdultPassenger | validTotalInfantPassenger |
    And [desktop-rail-search-page] user click cari
    Then [desktop-rail-list-page] user should see route departure station
    And [desktop-rail-list-page] user should see route destination station
    And [desktop-rail-list-page] user should see list departure station
    And [desktop-rail-list-page] user should see list destination station
    And [desktop-rail-list-page] user should see total passenger

    When [desktop-rail-list-page] user click choose departure ticket
    Then [desktop-rail-list-page] user should see ticket summary departure

    When [desktop-rail-list-page] user click choose return ticket
    Then [desktop-rail-list-page] user should see ticket summary return

    When [desktop-rail-list-page] user click order
    Then [desktop-rail-checkout-page] user should see order detail
    And [desktop-rail-checkout-page] user should see origin station
    And [desktop-rail-checkout-page] user should see destination station
    And [desktop-rail-checkout-page] user should see ticket price
    And [desktop-rail-checkout-page] user should see total ticket price
    And [desktop-rail-checkout-page] user should see total passenger
    And [desktop-rail-checkout-page] user should see date