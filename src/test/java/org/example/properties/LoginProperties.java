package org.example.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("org.example.properties.LoginProperties")
@Data
@ConfigurationProperties(prefix = "login")
public class LoginProperties {
    private HashMap<String, String> data;

    public String get(String key) {
        return data.get(key);
    }
}