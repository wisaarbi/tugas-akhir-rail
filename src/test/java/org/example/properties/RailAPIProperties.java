package org.example.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("org.example.properties.RailAPIProperties")
@Data
@ConfigurationProperties(prefix = "rail-api")
public class RailAPIProperties {
    private HashMap<String, String> data;

    public String get(String key) {
        return data.get(key);
    }
}
