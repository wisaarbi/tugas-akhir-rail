package org.example.drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class CustomFirefoxDriver extends CustomWebDriver {

    FirefoxProfile profile;
    FirefoxOptions options;
    DesiredCapabilities capabilities;

    public CustomFirefoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        profile = new FirefoxProfile();
        options = new FirefoxOptions();
        capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
    }

    @Override
    public void initializeLocalDriver() {
        options.merge(capabilities);
        driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
    }

    @Override
    public WebDriver getDriver() {
        return driver;
    }
}
