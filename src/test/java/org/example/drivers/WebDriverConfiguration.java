package org.example.drivers;

import org.example.utility.EnvironmentVariablesUtility;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;

public class WebDriverConfiguration implements DriverSource {

    @Override
    public WebDriver newDriver() {
        CustomBrowser browser = CustomBrowser.valueOf(EnvironmentVariablesUtility.getProperties("browser").toUpperCase());
        CustomWebDriver customWebDriver;
        switch (browser) {
            case CHROME:
                customWebDriver = new CustomChromeDriver();
                break;
            case FIREFOX:
                customWebDriver = new CustomFirefoxDriver();
                break;
            default:
                customWebDriver = new CustomChromeDriver();
                break;
        }
        customWebDriver.initializeLocalDriver();
        return customWebDriver.getDriver();
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
