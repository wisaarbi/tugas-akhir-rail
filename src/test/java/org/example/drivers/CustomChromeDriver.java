package org.example.drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class CustomChromeDriver extends CustomWebDriver {

    ChromeOptions options;
    DesiredCapabilities capabilities;

    public CustomChromeDriver() {
        WebDriverManager.chromedriver().setup();
        capabilities = DesiredCapabilities.chrome();
        options = new ChromeOptions();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
    }

    @Override
    public void initializeLocalDriver() {
        options.merge(capabilities);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Override
    public WebDriver getDriver() {
        return driver;
    }
}
