package org.example.drivers;

import org.openqa.selenium.WebDriver;

public abstract class CustomWebDriver {
    WebDriver driver;
    public abstract void initializeLocalDriver();
    public abstract WebDriver getDriver();
}
