package org.example.controller;

import io.restassured.response.Response;
import org.example.responses.railavailability.getsearchpopularstation.GetSearchPopularStationResponse;
import org.springframework.stereotype.Component;

@Component("org.example.controller.SearchPopularStationController")
public class SearchPopularStationController extends ServiceApi {

    public GetSearchPopularStationResponse getSearchPopularStation() {
        Response response =  services("blibli")
                .when().get("/travel/tiket-kereta-api/search-popular-station");
        response.prettyPrint();
        return response.getBody().as(GetSearchPopularStationResponse.class);
    }
}
