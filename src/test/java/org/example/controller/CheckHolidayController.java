package org.example.controller;

import io.restassured.response.Response;
import org.example.data.CheckHolidayAPIData;
import org.example.responses.checkholiday.getcheckholiday.GetCheckHolidayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("org.example.controller.CheckHolidayController")
public class CheckHolidayController extends ServiceApi {

    @Autowired
    CheckHolidayAPIData checkHolidayAPIData;

    public GetCheckHolidayResponse getCheckHoliday() {
        Response response =  services("blibli")
                .queryParam("yearStart", checkHolidayAPIData.getYearStart())
                .queryParam("monthStart", checkHolidayAPIData.getMonthStart())
                .queryParam("yearEnd", checkHolidayAPIData.getYearEnd())
                .queryParam("monthEnd", checkHolidayAPIData.getMonthEnd())
                .when().get("/backend/travel/holiday");
        response.prettyPrint();
        return response.getBody().as(GetCheckHolidayResponse.class);
    }
}
