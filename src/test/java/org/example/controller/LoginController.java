package org.example.controller;

import io.restassured.response.Response;
import org.example.data.LoginAPIData;
import org.example.responses.login.getuserlogin.GetUserLoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("org.example.controller.LoginController")
public class LoginController extends ServiceApi {

    @Autowired
    LoginAPIData loginAPIData;

    public GetUserLoginResponse getUserLogin() {
        Response response =  services("blibli")
                .cookies(loginAPIData.getLoginCookies())
                .when().get("/backend/common/users");
        response.prettyPrint();
        return response.getBody().as(GetUserLoginResponse.class);
    }
}
