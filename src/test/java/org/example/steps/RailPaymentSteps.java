package org.example.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.data.RailData;
import org.example.pages.desktop.DesktopRailPaymentPage;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class RailPaymentSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailPaymentPage desktopRailPaymentPage;

    @Autowired
    RailData railData;

    @And("[desktop-rail-payment-page] user click passenger details")
    public void desktopRailPaymentPageUserClickPassengerDetails() {
        desktopRailPaymentPage.clickPassengerDetails();
    }

    @Then("[desktop-rail-payment-page] user should see carriage that choosed")
    public void desktopRailPaymentPageUserShouldSeeCarriageThatChoosed() {
        for (int i = 0; i < railData.getCarriageDeparture().size(); i++) {
            assertThat("carriage departure is different with expected", desktopRailPaymentPage.getSeatPassengerDeparture().get(i),
                    containsString(railData.getCarriageDeparture().get(i)));
        }
        if (railData.getDateReturn() != null) {
            for (int i = 0; i < railData.getCarriageReturn().size(); i++) {
                assertThat("carriage return is different with expected", desktopRailPaymentPage.getSeatPassengerReturn().get(i),
                        containsString(railData.getCarriageReturn().get(i)));
            }
        }
    }

    @And("[desktop-rail-payment-page] user should see seat that choosed")
    public void desktopRailPaymentPageUserShouldSeeSeatThatChoosed() {
        for (int i = 0; i < railData.getSeatDeparture().size(); i++) {
            assertThat("seat departure is different with expected", desktopRailPaymentPage.getSeatPassengerDeparture().get(i),
                    containsString(railData.getSeatDeparture().get(i)));
        }
        if (railData.getDateReturn() != null) {
            for (int i = 0; i < railData.getSeatReturn().size(); i++) {
                assertThat("seat return is different with expected", desktopRailPaymentPage.getSeatPassengerReturn().get(i),
                        containsString(railData.getSeatReturn().get(i)));
            }
        }
    }

    @Then("[desktop-rail-payment-page] user should see bill details")
    public void desktopRailPaymentPageUserShouldSeeBillDetails() {
        assertThat("bill details is not visible", desktopRailPaymentPage.isBillDetailsVisible(),
                equalTo(true));
    }

    @And("[desktop-rail-payment-page] user should see trip details")
    public void desktopRailPaymentPageUserShouldSeeTripDetails() {
        assertThat("trip details is not visible", desktopRailPaymentPage.isTripDetailsVisible(),
                equalTo(true));
    }

    @And("[desktop-rail-payment-page] user should see passenger details")
    public void desktopRailPaymentPageUserShouldSeePassengerDetails() {
        assertThat("trip details is not visible", desktopRailPaymentPage.isPassengerDetailsVisible(),
                equalTo(true));
    }

    @When("[desktop-rail-payment-page] user fill payment method")
    public void desktopRailPaymentPageUserFillPaymentMethod() {
        desktopRailPaymentPage.fillPaymentMethod();
    }
    
    @When("[desktop-rail-payment-page] user click cancel order")
    public void desktopRailPaymentPageUserClickCancelOrder() {
        desktopRailPaymentPage.clickCancelOrder();
    }

    @And("[desktop-rail-payment-page] user click confirm cancel order")
    public void desktopRailPaymentPageUserClickConfirmCancelOrder() {
        desktopRailPaymentPage.clickConfirmCancelOrder();
    }

    @Then("[desktop-rail-payment-page] user see a message cancel order was successful")
    public void desktopRailPaymentPageUserSeeAMessageCancelOrderWasSuccessful() {
        assertThat("message cancel order was successful is not visible", desktopRailPaymentPage.isMessageCancelOrderVisible(),
                equalTo(true));
    }
}
