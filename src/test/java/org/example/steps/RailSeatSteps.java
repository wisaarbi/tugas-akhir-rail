package org.example.steps;

import io.cucumber.java.en.And;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.data.RailData;
import org.example.pages.desktop.DesktopRailSeatPage;
import org.springframework.beans.factory.annotation.Autowired;

public class RailSeatSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailSeatPage desktopRailSeatPage;

    @Autowired
    RailData railData;

    @And("[desktop-rail-seat-page] user choose seat")
    public void desktopRailSeatPageUserChooseSeat() {
        for (int i = 0; i < railData.getTotalAdultPassenger(); i++) {
            desktopRailSeatPage.choosePassenger(i)
                    .chooseCarriage()
                    .chooseSeat();
        }
        railData.setCarriageDeparture(desktopRailSeatPage.getSelectedCarriage());
        railData.setSeatDeparture(desktopRailSeatPage.getSelectedSeat());
        if (railData.getDateReturn() != null) {
            desktopRailSeatPage.clickSelectReturnSeat();
            for (int i = 0; i < railData.getTotalAdultPassenger(); i++) {
                desktopRailSeatPage.choosePassenger(i)
                        .chooseCarriage()
                        .chooseSeat();
            }
            railData.setCarriageReturn(desktopRailSeatPage.getSelectedCarriage());
            railData.setSeatReturn(desktopRailSeatPage.getSelectedSeat());
        }
    }

    @And("[desktop-rail-seat-page] user click proceed to payment")
    public void desktopRailSeatPageUserClickProceedToPayment() {
        desktopRailSeatPage.clickProceedToPayment();
    }

}
