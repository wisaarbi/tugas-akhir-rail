package org.example.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.controller.CheckHolidayController;
import org.example.data.CheckHolidayAPIData;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CheckHolidaySteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    CheckHolidayAPIData checkHolidayAPIData;

    @Autowired
    CheckHolidayController checkHolidayController;

    @Given("[check-holiday-api] user prepare data get check holiday with year start {int}")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithYearStart(int yearStart) {
        checkHolidayAPIData.setYearStart(yearStart);
    }

    @And("[check-holiday-api] user prepare data get check holiday with month start {int}")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithMonthStart(int monthStart) {
        checkHolidayAPIData.setMonthStart(monthStart);
    }

    @And("[check-holiday-api] user prepare data get check holiday with year end {int}")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithYearEnd(int yearEnd) {
        checkHolidayAPIData.setYearEnd(yearEnd);
    }

    @And("[check-holiday-api] user prepare data get check holiday with month end {int}")
    public void checkHolidayApiUserPrepareDataGetCheckHolidayWithMonthEnd(int monthEnd) {
        checkHolidayAPIData.setMonthEnd(monthEnd);
    }

    @When("[check-holiday-api] user get check holiday")
    public void checkHolidayApiUserGetCheckHoliday() {
        checkHolidayAPIData.setResponseGetCheckHoliday(checkHolidayController.getCheckHoliday());
    }

    @Then("[check-holiday-api] user should see get check holiday response with status code {string}")
    public void checkHolidayApiUserShouldSeeGetCheckHolidayResponseWithStatusCode(String statusCode) {
        assertThat("status code is different with expected", checkHolidayAPIData.getResponseGetCheckHoliday().getCode(),
                equalTo(statusCode));
    }

    @And("[check-holiday-api] user should see get check holiday response with status message {string}")
    public void checkHolidayApiUserShouldSeeGetCheckHolidayResponseWithStatusMessage(String statusMessage) {
        assertThat("status message is different with expected", checkHolidayAPIData.getResponseGetCheckHoliday().getMessage(),
                equalTo(statusMessage));
    }
}
