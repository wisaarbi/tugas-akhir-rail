package org.example.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.controller.LoginController;
import org.example.data.LoginAPIData;
import org.example.data.LoginData;
import org.example.pages.desktop.DesktopFavouritesPage;
import org.example.pages.desktop.DesktopHeaderMainPage;
import org.example.pages.desktop.DesktopHeaderPage;
import org.example.pages.desktop.DesktopHeaderTravelPage;
import org.example.pages.desktop.DesktopHomePage;
import org.example.pages.desktop.DesktopLoginPage;
import org.example.pages.desktop.DesktopProfilePage;
import org.example.pages.desktop.DesktopRailSearchPage;
import org.example.properties.LoginProperties;
import org.example.utility.FetchingEmail;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LoginSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopHomePage desktopHomePage;

    @Autowired
    DesktopFavouritesPage desktopFavouritesPage;

    @Autowired
    DesktopHeaderTravelPage desktopHeaderTravelPage;

    @Autowired
    DesktopLoginPage desktopLoginPage;

    @Autowired
    DesktopHeaderMainPage desktopHeaderMainPage;

    @Autowired
    DesktopProfilePage desktopProfilePage;

    @Autowired
    LoginProperties loginProperties;

    @Autowired
    LoginData loginData;

    @Autowired
    DesktopHeaderPage desktopHeaderPage;

    @Autowired
    FetchingEmail fetchingEmail;

    @Autowired
    DesktopRailSearchPage desktopRailSearchPage;

    @Autowired
    LoginAPIData loginAPIData;

    @Autowired
    LoginController loginController;

    @Given("[desktop-home-page] user open home page blibli")
    public void desktopHomePageUserOpenHomepageBlibli() {
        desktopHomePage.open();
        desktopHomePage.addDefaultCookies();
    }

    @And("[desktop-home-page] user click all category")
    public void desktopHomePageUserClickAllCategory() {
        desktopHomePage.clickAllCategory();
        if (desktopHomePage.isIframeVisible()) {
            desktopHomePage.closeIframeIfShowUp();
        }
    }

    @And("[desktop-favourites-page] user click kereta")
    public void desktopFavouritesPageUserClickKereta() {
        desktopFavouritesPage.clickKereta();
    }

    @Then("[desktop-rail-search-page] user should see rail search form")
    public void desktopRailSearchPageUserShouldSeeRailSearchForm() {
        assertThat("rail search form is not visible",
                desktopRailSearchPage.isRailSearchFormVisible(), equalTo(true));
    }

    @When("[desktop-header-travel-page] user click login")
    public void desktopHeaderTravelPageUserClickLogin() {
        desktopHeaderTravelPage.clickLogin();
    }

    @Then("[desktop-login-page] user should see login form")
    public void desktopLoginPageUserShouldSeeLoginForm() {
        assertThat("login form not visible", desktopLoginPage.isLoginFormVisible(), equalTo(true));
    }

    @When("[desktop-login-page] user fill email with {string}")
    public void desktopLoginPageUserFillEmail(String email) {
        switch (email) {
            case "validEmail":
                loginData.setEmail(loginProperties.get("validEmail"));
                break;
            case "notRegisteredEmail":
                loginData.setEmail(loginProperties.get("notRegisteredEmail"));
                break;
            case "invalidEmail":
                loginData.setEmail(loginProperties.get("invalidEmail"));
                break;
            default:
                loginData.setEmail(email);
                break;
        }
        desktopLoginPage.fillEmail(loginData.getEmail());
    }

    @And("[desktop-login-page] user fill password with {string}")
    public void desktopLoginPageUserFillPassword(String password) {
        switch (password) {
            case "validPassword":
                loginData.setPassword(loginProperties.get("validPassword"));
                break;
            case "invalidPassword":
                loginData.setPassword(loginProperties.get("invalidPassword"));
                break;
            default:
                loginData.setPassword(password);
                break;
        }
        desktopLoginPage.fillPassword(loginData.getPassword());
    }

    @And("[desktop-login-page] user click login")
    public void desktopLoginPageUserClickLogin() {
        desktopLoginPage.clickLogin();
    }

    @And("[desktop-header-main-page] user click profile")
    public void desktopHeadermainPageUserClickProfile() {
        desktopHeaderMainPage.clickProfile();
    }

    @Then("[desktop-profile-page] user should see the same email")
    public void desktopProfilePageUserShouldSeeTheSameEmail() {
        assertThat("email is different", desktopProfilePage.getEmail(), equalTo(loginData.getEmail()));
    }

    @And("[desktop-profile-page] user open profile page")
    public void desktopProfilePageUserOpenProfile() {
        desktopProfilePage.open();
    }

    @Then("[desktop-login-page] user should see error message {string}")
    public void desktopLoginPageUserShouldSeeErrorMessage(String errorMessage) {
        assertThat("error message not visible", desktopLoginPage.isErrorMessageVisible(), equalTo(true));
        assertThat("error message is different with expected", desktopLoginPage.getErrorMessage(), equalTo(errorMessage));
    }

    @And("[desktop-home-page] user close iframe if show up")
    public void desktopHomePageUserCloseIframeIfShowUp() {
        if (desktopHomePage.isIframeVisible()) {
            desktopHomePage.closeIframeIfShowUp();
        }
    }

    @Then("[desktop-login-page] user should see button login is {string}")
    public void desktopLoginPageUserShouldSeeButtonLoginIs(String buttonStatus) {
        if (buttonStatus.equals("enabled")) {
            assertThat("button login is not enabled", desktopLoginPage.isButtonLoginEnabled(), equalTo(true));
        } else {
            assertThat("button login is not disabled", desktopLoginPage.isButtonLoginEnabled(), equalTo(false));
        }
    }

    @And("[desktop-login-page] user click send verification code via email")
    public void desktopLoginPageUserClickSendVerificationCodeViaEmail() {
        desktopLoginPage.clickVerificationCodeViaEmail();
    }

    @Given("[common-page] user open new tab")
    public void commonPageUserOpenNewTab() {
        desktopLoginPage.openNewTab();
    }

    @And("[common-page] user take control of the new tab")
    public void commonPageUserTakeControlOfTheNewTab() {
        desktopLoginPage.takeControlOfTheNewTab();
    }

    @And("[common-page] user close the new tab")
    public void commonPageUserCloseTheNewTab() {
        desktopHomePage.closeTheNewTab();
    }

    @And("[common-page] user take control of the old tab")
    public void commonPageUserTakeControlOfTheOldTab() {
        desktopHomePage.takeControlOfTheOldTab();
    }

    @When("[desktop-login-page] user fill the verification code")
    public void desktopLoginPageUserFillTheVerificationCode() {
        desktopLoginPage.fillVerificationCode(loginData.getVerificationCode());
    }

    @And("[desktop-login-page] user click verification")
    public void desktopLoginPageUserClickVerification() {
        desktopLoginPage.clickVerification();
    }

    @Then("[desktop-header-page] user should see user already login")
    public void desktopHeaderPageUserShouldSeeUserAlreadyLogin() {
        assertThat("user already login is not visible", desktopHeaderPage.isUserAlreadyLoginVisible(),
                equalTo(true));
    }

    @And("[desktop-login-page] user gets a verification code with")
    public void desktopLoginPageUserGetsAVerificationCodeWith(DataTable dt) {
        waitABit(3000);
        List<Map<String, String>> params = dt.asMaps(String.class, String.class);
        String email = params.get(0).get("email");
        switch (email) {
            case "validEmailGoogle":
                email = loginProperties.get("validEmailGoogle");
                break;
            default:
                break;
        }
        String password = params.get(0).get("password");
        switch (password) {
            case "validPasswordGoogle":
                password = loginProperties.get("validPasswordGoogle");
                break;
            default:
                break;
        }
        String host = params.get(0).get("host");
        String mailStoreType = params.get(0).get("mailStoreType");
        String port = params.get(0).get("port");
        loginData.setVerificationCode(fetchingEmail.getVerificationCode(host, port, mailStoreType, email, password));
    }

    @And("[desktop-home-page] user decline location permission")
    public void desktopHomePageUserDeclineLocationPermission() {
        desktopHomePage.declineLocationPermission();
    }

    @Given("[desktop-profile-page] user get cookies")
    public void desktopProfilePageUserGetCookies() {
        loginData.setLoginCookies(desktopProfilePage.getCookies());
    }

    @And("[login-api] user prepare data get user has logged in with cookies")
    public void loginApiUserPrepareDataGetUserHasLoggedInWithCookies() {
        loginAPIData.setLoginCookies(loginData.getLoginCookies());
    }

    @When("[login-api] user send get user has logged in request")
    public void loginApiUserSendGetUserHasLoggedInRequest() {
        loginAPIData.setResponseGetUserLogin(loginController.getUserLogin());
    }

    @Then("[login-api] user should see get user has logged in response with status code {int}")
    public void loginApiUserShouldSeeGetUserHasLoggedInStatusCode(int statusCode) {
        assertThat("status code is different", loginAPIData.getResponseGetUserLogin().getCode(), equalTo(statusCode));
    }

    @And("[login-api] user should see get user has logged in response with email {string}")
    public void loginApiUserShouldSeeGetUserHasLoggedInEmail(String email) {
        switch (email) {
            case "validEmail":
                email = loginProperties.get("validEmail");
                break;
            case "invalidEmail":
                email = loginProperties.get("invalidEmail");
                break;
            default:
                break;
        }
        assertThat("email is different", loginAPIData.getResponseGetUserLogin().getData().getEmail(), equalTo(email));
    }

    @And("[login-api] user should see get user has logged in response with status message {string}")
    public void loginApiUserShouldSeeGetUserHasLoggedInStatusMessage(String statusMessage) {
        assertThat("status message is different", loginAPIData.getResponseGetUserLogin().getStatus(), equalTo(statusMessage));
    }
}
