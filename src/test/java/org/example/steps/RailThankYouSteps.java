package org.example.steps;

import io.cucumber.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.pages.desktop.DesktopRailThankYouPage;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RailThankYouSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailThankYouPage desktopRailThankYouPage;

    @Then("[desktop-rail-thankyou-page] user should see thank you section")
    public void desktopRailThankyouPageUserShouldSeeThankYouSection() {
        assertThat("thankyou section is not visible", desktopRailThankYouPage.isThankYouSectionVisible(),
                equalTo(true));
    }

}
