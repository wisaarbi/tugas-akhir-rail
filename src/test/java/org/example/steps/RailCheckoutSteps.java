package org.example.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.apache.commons.lang3.StringUtils;
import org.example.data.RailData;
import org.example.pages.desktop.DesktopRailCheckoutPage;
import org.example.properties.RailProperties;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class RailCheckoutSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    RailProperties railProperties;

    @Autowired
    DesktopRailCheckoutPage desktopRailCheckoutPage;

    @Autowired
    RailData railData;

    @Then("[desktop-rail-checkout-page] user should see order detail")
    public void desktopRailCheckoutPageUserShouldSeeOrderDetail() {
        assertThat("order detail is not visible", desktopRailCheckoutPage.isOrderDetailVisible(), equalTo(true));
    }

    @And("[desktop-rail-checkout-page] user should see origin station")
    public void desktopRailCheckoutPageUserShouldSeeOriginStation() {
        assertThat("departure origin station is different with expected", desktopRailCheckoutPage.getDepartureOriginStation(),
                containsString(railData.getDeptStation()));
        if (railData.getDateReturn() != null) {
            assertThat("return origin station is different with expected", desktopRailCheckoutPage.getReturnOriginStation(),
                    containsString(railData.getDestStation()));
        }
    }

    @And("[desktop-rail-checkout-page] user should see destination station")
    public void desktopRailCheckoutPageUserShouldSeeDestinationStation() {
        assertThat("departure destination station is different with expected", desktopRailCheckoutPage.getDepartureDestinationStation(),
                containsString(railData.getDestStation()));
        if (railData.getDateReturn() != null) {
            assertThat("return destination station is different with expected", desktopRailCheckoutPage.getReturnDestinationStation(),
                    containsString(railData.getDeptStation()));
        }
    }

    @And("[desktop-rail-checkout-page] user should see ticket price")
    public void desktopRailCheckoutPageUserShouldSeeTicketPrice() {
        assertThat("departure ticket price is not visible", desktopRailCheckoutPage.isDepartureTicketPriceVisible(),
                equalTo(true));
        if (railData.getDateReturn() != null) {
            assertThat("return ticket price is not visible", desktopRailCheckoutPage.isReturnTicketPriceVisible(),
                    equalTo(true));
        }
    }

    @And("[desktop-rail-checkout-page] user should see total ticket price")
    public void desktopRailCheckoutPageUserShouldSeeTotalTicketPrice() {
        assertThat("total ticket price is not visible", desktopRailCheckoutPage.isTotalTicketPriceVisible(),
                equalTo(true));
    }

    @And("[desktop-rail-checkout-page] user should see total passenger")
    public void desktopRailCheckoutPageUserShouldSeeTotalPassenger() {
        String[] passennger = StringUtils.substringBetween(desktopRailCheckoutPage.getTotalPassenger(), "(", ")").split(", ");
        String actualAdultPassenger = passennger[0];
        String actualInfantPassenger = passennger[1];

        String expectedAdultPassenger = String.valueOf(railData.getTotalAdultPassenger());
        String expectedInfantPassenger = String.valueOf(railData.getTotalInfantPassenger());

        assertThat("total adult passenger is different with expected", actualAdultPassenger,
                containsString(expectedAdultPassenger));
        assertThat("total infant passenger is different with expected", actualInfantPassenger,
                containsString(expectedInfantPassenger));
    }

    @And("[desktop-rail-checkout-page] user should see train class")
    public void desktopRailCheckoutPageUserShouldSeeTrainClass() {
        assertThat("departure train class is different with expected", desktopRailCheckoutPage.getDepartureTrainClass(),
                containsString(railData.getClassDeparture()));
        if (railData.getDateReturn() != null) {
            assertThat("return train class is different with expected", desktopRailCheckoutPage.getReturnTrainClass(),
                    containsString(railData.getClassReturn()));
        }
    }

    @And("[desktop-rail-checkout-page] user should see train type")
    public void desktopRailCheckoutPageUserShouldSeeTrainType() {
        assertThat("departure train type is different with expected", desktopRailCheckoutPage.getDepartureTrainType(),
                containsString(railData.getTrainTypeDeparture()));
        if (railData.getDateReturn() != null) {
            assertThat("return train type is different with expected", desktopRailCheckoutPage.getReturnTrainType(),
                    containsString(railData.getTrainTypeReturn()));
        }
    }

    @And("[desktop-rail-checkout-page] user should see date")
    public void desktopRailCheckoutPageUserShouldSeeDate() {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatterDateDeparture = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateDeparture = LocalDate.parse(railData.getDateDeparture(), formatterDateDeparture);
        formatterDateDeparture = DateTimeFormatter.ofPattern("E, d MMM yyyy", locale);
        String expectedDateDeparture = localDateDeparture.format(formatterDateDeparture);

        if (expectedDateDeparture.contains("Agu")) {
            expectedDateDeparture = expectedDateDeparture.replace("Agu", "Ags");
        }

        assertThat("departure date is different with expected", desktopRailCheckoutPage.getDepartureDate(),
                equalTo(expectedDateDeparture));

        if (railData.getDateReturn() != null) {
            DateTimeFormatter formatterDateReturn = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
            LocalDate localDateReturn = LocalDate.parse(railData.getDateReturn(), formatterDateReturn);
            formatterDateReturn = DateTimeFormatter.ofPattern("E, d MMM yyyy", locale);
            String expectedDateReturn = localDateReturn.format(formatterDateReturn);

            if (expectedDateReturn.contains("Agu")) {
                expectedDateReturn = expectedDateReturn.replace("Agu", "Ags");
            }

            assertThat("return date is different with expected", desktopRailCheckoutPage.getReturnDate(),
                    equalTo(expectedDateReturn));
        }
    }

    @When("[desktop-rail-checkout-page] user fill customer title {string}")
    public void desktopRailCheckoutPageUserFillCustomerTitle(String customerTitle) {
        switch (customerTitle) {
            case "customerTitle":
                railData.setCustomerTitle(railProperties.get("customerTitle"));
                break;
            default:
                railData.setCustomerTitle(customerTitle);
                break;
        }
        desktopRailCheckoutPage.fillCustomerTitle(railData.getCustomerTitle());
    }

    @And("[desktop-rail-checkout-page] user fill customer name {string}")
    public void desktopRailCheckoutPageUserFillCustomerName(String customerName) {
        switch (customerName) {
            case "validCustomerName":
                railData.setCustomerName(railProperties.get("validCustomerName"));
                break;
            default:
                railData.setCustomerName(customerName);
                break;
        }
        desktopRailCheckoutPage.fillCustomerName(railData.getCustomerName());
    }

    @And("[desktop-rail-checkout-page] user fill customer phone number {string}")
    public void desktopRailCheckoutPageUserFillCustomerPhoneNumber(String customerPhoneNumber) {
        switch (customerPhoneNumber) {
            case "validCustomerPhoneNumber":
                railData.setCustomerPhoneNumber(railProperties.get("validCustomerPhoneNumber"));
                break;
            default:
                railData.setCustomerPhoneNumber(customerPhoneNumber);
                break;
        }
        desktopRailCheckoutPage.fillCustomerPhoneNumber(railData.getCustomerPhoneNumber());
    }

    @And("[desktop-rail-checkout-page] user fill customer email {string}")
    public void desktopRailCheckoutPageUserFillCustomerEmail(String customerEmail) {
        switch (customerEmail) {
            case "validCustomerEmail":
                railData.setCustomerEmail(railProperties.get("validCustomerEmail"));
                break;
            default:
                railData.setCustomerEmail(customerEmail);
                break;
        }
        desktopRailCheckoutPage.fillCustomerEmail(railData.getCustomerEmail());
    }

    @And("[desktop-rail-checkout-page] user click continue to fill in passenger data")
    public void desktopRailCheckoutPageUserClickContinueToFillInPassengerData() {
        desktopRailCheckoutPage.clickContinueToFillInPassengerData();
        desktopRailCheckoutPage.clickAcceptTNCCOVID();
    }

    @Then("[desktop-rail-checkout-page] user should see customer title")
    public void desktopRailCheckoutPageUserShouldSeeCustomerTitle() {
        assertThat("customer title is different with expected", desktopRailCheckoutPage.getCustomerTitle(),
                equalTo(railData.getCustomerTitle()));
    }

    @And("[desktop-rail-checkout-page] user should see customer name")
    public void desktopRailCheckoutPageUserShouldSeeCustomerName() {
        assertThat("customer name is different with expected", desktopRailCheckoutPage.getCustomerName(),
                equalTo(railData.getCustomerName()));
    }

    @And("[desktop-rail-checkout-page] user should see customer phone number")
    public void desktopRailCheckoutPageUserShouldSeeCustomerPhoneNumber() {
        assertThat("customer phone number is different with expected", desktopRailCheckoutPage.getCustomerPhoneNumber(),
                equalTo(railData.getCustomerPhoneNumber()));
    }

    @And("[desktop-rail-checkout-page] user should see customer email")
    public void desktopRailCheckoutPageUserShouldSeeCustomerEmail() {
        assertThat("customer email is different with expected", desktopRailCheckoutPage.getCustomerEmail(),
                equalTo(railData.getCustomerEmail()));
    }

    @Then("[desktop-rail-checkout-page] user should see error message {string} on customer section")
    public void desktopRailCheckoutPageUserShouldSeeErrorMessageOnCustomerSection(String errorMessage) {
        assertThat("error message is different with expected", desktopRailCheckoutPage.getErrorMessageOnCustomerSection(),
                equalTo(errorMessage));
    }

    @When("[desktop-rail-checkout-page] user click same as customer")
    public void desktopRailCheckoutPageUserClickSameAsCustomer() {
        desktopRailCheckoutPage.clickSameAsCustomer();
    }

    @Then("[desktop-rail-checkout-page] user should see title adult passenger is same as customer")
    public void desktopRailCheckoutPageUserShouldSeeTitleAdultPassengerIsSameAsCustomer() {
        assertThat("title adult passenger is different with expected", desktopRailCheckoutPage.getTitlePassengerSameAsCustomer(),
                equalTo(railData.getCustomerTitle()));
    }

    @And("[desktop-rail-checkout-page] user should see name adult passenger is same as customer")
    public void desktopRailCheckoutPageUserShouldSeeNameAdultPassengerIsSameAsCustomer() {
        assertThat("name adult passenger is different with expected", desktopRailCheckoutPage.getNamePassengerSameAsCustomer(),
                equalTo(railData.getCustomerName()));
    }

    @When("[desktop-rail-checkout-page] user fill adult passenger data")
    public void desktopRailCheckoutPageUserFillAdultPassengerData() {
        desktopRailCheckoutPage.fillAdultPassenger(railData.getTotalAdultPassenger());
    }

    @Then("[desktop-rail-checkout-page] user should see adult passenger data")
    public void desktopRailCheckoutPageUserShouldSeeAdultPassengerData() {
        assertThat("adult passenger data is not visible", desktopRailCheckoutPage.isAdultPassengerDataVisible(),
                equalTo(true));
    }

    @When("[desktop-rail-checkout-page] user fill infant passenger data")
    public void desktopRailCheckoutPageUserFillInfantPassengerData() {
        desktopRailCheckoutPage.fillInfantPassenger(railData.getTotalInfantPassenger());
    }

    @Then("[desktop-rail-checkout-page] user should see infant passenger data")
    public void desktopRailCheckoutPageUserShouldSeeInfantPassengerData() {
        assertThat("infant passenger data is not visible", desktopRailCheckoutPage.isInfantPassengerDataVisible(),
                equalTo(true));
    }

    @When("[desktop-rail-checkout-page] user click change seat")
    public void desktopRailCheckoutPageUserClickChangeSeat() {
        desktopRailCheckoutPage.clickChangeSeat();
    }

    @When("[desktop-rail-checkout-page] user click proceed to payment")
    public void desktopRailCheckoutPageUserClickProceedToPayment() {
        desktopRailCheckoutPage.clickProceedToPayment();
    }
}
