package org.example.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.controller.RailAvailabilityController;
import org.example.controller.SearchPopularStationController;
import org.example.data.RailAPIData;
import org.example.data.RailData;
import org.example.pages.desktop.DesktopRailListPage;
import org.example.pages.desktop.DesktopRailSearchPage;
import org.example.properties.RailAPIProperties;
import org.example.properties.RailProperties;
import org.example.responses.railavailability.getrailticketavailability.DepartureRailListItem;
import org.example.responses.railavailability.getsearchpopularstation.CityListItem;
import org.example.utility.DateUtility;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;

public class RailSearchTicketSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    DesktopRailSearchPage desktopRailSearchPage;

    @Autowired
    DesktopRailListPage desktopRailListPage;

    @Autowired
    RailProperties railProperties;

    @Autowired
    RailData railData;

    @Autowired
    DateUtility dateUtility;

    @Autowired
    RailAPIProperties railAPIProperties;

    @Autowired
    RailAPIData railAPIData;

    @Autowired
    RailAvailabilityController railAvailabilityController;

    @Autowired
    SearchPopularStationController searchPopularStationController;

    @When("[desktop-rail-search-page] user fill departure city {string}")
    public void desktopRailSearchPageUserFillDepartureCity(String deptCity) {
        switch (deptCity) {
            case "validDeptCity":
                railData.setDeptCity(railProperties.get("validDeptCity"));
                break;
            case "invalidDeptCity":
                railData.setDeptCity(railProperties.get("invalidDeptCity"));
                break;
            default:
                railData.setDeptCity(deptCity);
                break;
        }
        desktopRailSearchPage.fillDepartureCity(railData.getDeptCity());
    }

    @And("[desktop-rail-search-page] user fill departure station {string}")
    public void desktopRailSearchPageUserFillDepartureStation(String deptStation) {
        switch (deptStation) {
            case "validDeptStation":
                railData.setDeptStation(railProperties.get("validDeptStation"));
                break;
            default:
                railData.setDeptStation(deptStation);
                break;
        }
        desktopRailSearchPage.fillDepartureStation(railData.getDeptStation());
    }

    @And("[desktop-rail-search-page] user fill destination city {string}")
    public void desktopRailSearchPageUserFillDestinationCity(String destCity) {
        switch (destCity) {
            case "validDestCity":
                railData.setDestCity(railProperties.get("validDestCity"));
                break;
            case "invalidDestCity":
                railData.setDestCity(railProperties.get("invalidDestCity"));
                break;
            default:
                railData.setDestCity(destCity);
                break;
        }
        desktopRailSearchPage.fillDestinationCity(railData.getDestCity());
    }

    @And("[desktop-rail-search-page] user fill destination station {string}")
    public void desktopRailSearchPageUserFillDestinationStation(String destStation) {
        switch (destStation) {
            case "validDestStation":
                railData.setDestStation(railProperties.get("validDestStation"));
                break;
            default:
                railData.setDestStation(destStation);
                break;
        }
        desktopRailSearchPage.fillDestinationStation(railData.getDestStation());
    }

    @And("[desktop-rail-search-page] user fill passenger")
    public void desktopRailSearchPageUserFillPassenger(DataTable passenger) {
        List<Map<String, String>> listTotalPassenger = passenger.asMaps(String.class, String.class);
        String totalAdultPassenger = listTotalPassenger.get(0).get("totalAdultPassenger");
        String totalInfantPassenger = listTotalPassenger.get(0).get("totalInfantPassenger");
        switch (totalAdultPassenger) {
            case "validTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validTotalAdultPassenger")));
                break;
            default:
                railData.setTotalAdultPassenger(Integer.parseInt(totalAdultPassenger));
                break;
        }
        switch (totalInfantPassenger) {
            case "validTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validTotalInfantPassenger")));
                break;
            default:
                railData.setTotalInfantPassenger(Integer.parseInt(totalInfantPassenger));
                break;
        }
        desktopRailSearchPage.clickPassenger()
                .fillPassenger(railData.getTotalAdultPassenger(), railData.getTotalInfantPassenger());
    }

    @And("[desktop-rail-search-page] user click cari")
    public void desktopRailSearchPageUserClickCari() {
        desktopRailSearchPage.clickCari();
    }

    @Then("[desktop-rail-search-page] user should see empty station error message {string}")
    public void desktopRailSearchPageUserShouldSeeEmptyStationErrorMessage(String emptyStationErrorMessage) {
        assertThat("empty station error message is not visible",
                desktopRailSearchPage.isEmptyStationErrorMessageVisible(), equalTo(true));
        assertThat("empty station error message is different with expected",
                desktopRailSearchPage.getEmptyStationErrorMessage(), equalTo(emptyStationErrorMessage));
    }

    @When("[desktop-rail-search-page] user click departure location")
    public void desktopRailSearchPageUserClickDepartureLocation() {
        desktopRailSearchPage.clickDepartureLocation();
    }

    @Then("[desktop-rail-search-page] user should see list popular city")
    public void desktopRailSearchPageUserShouldSeeListPopularCity() {
        assertThat("list popular city is not visible",
                desktopRailSearchPage.isListPopularCityVisible(), equalTo(true));
    }

    @And("[desktop-rail-search-page] user click destination location")
    public void desktopRailSearchPageUserClickDestinationLocation() {
        desktopRailSearchPage.clickDestinationLocation();
    }

    @Then("[desktop-rail-list-page] user should see route departure station")
    public void desktopRailListPageUserShouldSeeRouteDepartureStation() {
        assertThat("departure station is not visible",
                desktopRailListPage.isDepartureStationVisible(), equalTo(true));
        assertThat("departure station is different with expected",
                desktopRailListPage.getDepartureStation(), equalTo(railData.getDeptStation()));
    }

    @And("[desktop-rail-list-page] user should see route destination station")
    public void desktopRailListPageUserShouldSeeRouteDestinationStation() {
        assertThat("destination station is not visible",
                desktopRailListPage.isDestinationStationVisible(), equalTo(true));
        assertThat("destination station is different with expected",
                desktopRailListPage.getDestinationStation(), equalTo(railData.getDestStation()));
    }

    @And("[desktop-rail-list-page] user should see list departure station")
    public void desktopRailListPageUserShouldSeeListDepartureStation() {
        assertThat("list departure station is different with expected",
                desktopRailListPage.getListDepartureStation(), everyItem(equalTo(railData.getDeptStation())));
    }

    @And("[desktop-rail-list-page] user should see list destination station")
    public void desktopRailListPageUserShouldSeeListDestinationStation() {
        assertThat("list destination station is different with expected",
                desktopRailListPage.getListDestinationStation(), everyItem(equalTo(railData.getDestStation())));
    }

    @And("[desktop-rail-list-page] user should see total passenger")
    public void desktopRailListPageUserShouldSeeTotalPassenger() {
        String[] passenger = desktopRailListPage.getPassenger().split(", ");
        assertThat("adult passenger is different with expected",
                passenger[0], containsString(String.valueOf(railData.getTotalAdultPassenger())));
        if (railData.getTotalInfantPassenger() > 0) {
            assertThat("infant passenger is different with expected",
                    passenger[1], containsString(String.valueOf(railData.getTotalInfantPassenger())));
        }
    }

    @Then("[desktop-rail-search-page] user should see total passenger error message {string}")
    public void desktopRailSearchPageUserShouldSeeTotalPassengerErrorMessage(String totalPassengerErrorMessage) {
        assertThat("total passenger error message is not visible",
                desktopRailSearchPage.isTotalPassengerErrorMessageVisible(), equalTo(true));
        assertThat("total passenger error message is different with expected",
                desktopRailSearchPage.getTotalPassengerErrorMessage(), equalTo(totalPassengerErrorMessage));
    }

    @Then("[desktop-rail-search-page] user should see search error message")
    public void desktopRailSearchPageUserShouldSeeSearchErrorMessage() {
        assertThat("search error message is not visible",
                desktopRailSearchPage.isSearchErrorMessageVisible(), equalTo(true));
    }

    @And("[desktop-rail-search-page] user fill date of departure with day plus {int}")
    public void desktopRailSearchPageUserFillDateOfDepartureWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateDeparture(date);
        desktopRailSearchPage.clickDateDeparture().fillDateDeparture(railData.getDateDeparture());
    }

    @And("[desktop-rail-search-page] user fill date of return with day plus {int}")
    public void desktopRailSearchPageUserFillDateOfReturnWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateReturn(date);
        desktopRailSearchPage.clickDateReturn().fillDateReturn(railData.getDateReturn());
    }

    @Given("[rail-search-api] user prepare data get rail ticket availability with origin code {string}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithOriginCode(String originCode) {
        railAPIData.setOriginCode(originCode);
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with origin type {string}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithOriginType(String originType) {
        railAPIData.setOriginType(originType);
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with destination code {string}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDestinationCode(String destinationCode) {
        railAPIData.setDestinationCode(destinationCode);
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with destination type {string}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDestinationType(String destinationType) {
        railAPIData.setDestinationType(destinationType);
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with date departure {string}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDateDeparture(String dateDeparture) {
        if (dateDeparture.contains("day plus")) {
            int dayPlus = Integer.parseInt(dateDeparture.replace("day plus ", ""));
            Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
            String dateFormat = "yyyy-MM-dd";
            dateDeparture = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        }
        railAPIData.setDepartureDate(dateDeparture);
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with date return {string}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithDateReturn(String dateReturn) {
        switch (dateReturn) {
            case "validDateReturn":
                railAPIData.setReturnDate(railAPIProperties.get("validDateReturn"));
                break;
            default:
                railAPIData.setReturnDate(dateReturn);
                break;
        }
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with total adult passenger {int}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithTotalAdultPassenger(int totalAdult) {
        railAPIData.setAdult(totalAdult);
    }

    @And("[rail-search-api] user prepare data get rail ticket availability with total infant passenger {int}")
    public void railSearchApiUserPrepareDataGetRailTicketAvailabilityWithTotalInfantPassenger(int totalInfant) {
        railAPIData.setInfant(totalInfant);
    }

    @And("[rail-search-api] user should see get rail ticket availability response with total adult is same")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithTotalAdultIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<Integer> totalAdult = new ArrayList<>();
        for (DepartureRailListItem departureRailListItem : listDepartureRail)
            totalAdult.add(departureRailListItem.getAdult());
        assertThat("total adult is different with expected", totalAdult, everyItem(equalTo(railAPIData.getAdult())));
    }

    @And("[rail-search-api] user should see get rail ticket availability response with total infant is same")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithTotalInfantIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<Integer> totalInfant = new ArrayList<>();
        for (DepartureRailListItem departureRailListItem : listDepartureRail)
            totalInfant.add(departureRailListItem.getInfant());
        assertThat("total infant is different with expected", totalInfant, everyItem(equalTo(railAPIData.getInfant())));
    }

    @And("[rail-search-api] user should see get rail ticket availability response with message {string}")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithMessage(String message) {
        assertThat("message is different with expected",
                railAPIData.getResponseGetRailAvailability().getMessage(), equalTo(message));
    }

    @Given("[rail-search-api] user get search popular station")
    public void railSearchApiUserGetSearchPopularStation() {
        railAPIData.setResponseGetSearchPopularStation(searchPopularStationController.getSearchPopularStation());
    }

    @Then("[rail-search-api] user should see get search popular station response with status code {int}")
    public void railSearchApiUserShouldSeeGetSearchPopularStationResponseWithStatusCode(int statusCode) {
        assertThat("status code is different with expected", railAPIData.getResponseGetSearchPopularStation().getCode(),
                equalTo(statusCode));
    }

    @And("[rail-search-api] user should see get search popular station response with city name")
    public void railSearchApiUserShouldSeeGetSearchPopularStationResponseWithCityName(DataTable dt) {
        List<Map<String, String>> maps = dt.asMaps(String.class, String.class);
        List<String> expectedData = new ArrayList<>();
        for (Map<String, String> data : maps) {
            expectedData.add(data.get("cityName"));
        }

        List<String> actualData = new ArrayList<>();
        List<CityListItem> cityList = railAPIData.getResponseGetSearchPopularStation().getValue().getCityList();
        for (CityListItem data : cityList) {
            actualData.add(data.getCityName());
        }

        assertThat("city name is different with expected", actualData, equalTo(expectedData));
    }

    @When("[rail-search-api] user send get rail ticket availability request")
    public void railSearchApiUserSendGetRailTicketAvailabilityRequest() {
        railAPIData.setResponseGetRailAvailability(railAvailabilityController.getRailAvailability());
    }

    @Then("[rail-search-api] user should see get rail ticket availability response with status code {int}")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithStatusCode(int statusCode) {
        assertThat("status code is different with expected",
                railAPIData.getResponseGetRailAvailability().getCode(), equalTo(statusCode));
    }

    @And("[rail-search-api] user should see get rail ticket availability response with origin code is same")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithOriginCodeIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<String> originCode = new ArrayList<>();
        switch (railAPIData.getOriginType()) {
            case "STATION":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    originCode.add(departureRailListItem.getOrigin().getStationCode());
                break;
            case "CITY":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    originCode.add(departureRailListItem.getOrigin().getCity().getCityCode());
                break;
            default:
                break;
        }
        assertThat("origin code is different with expected", originCode, everyItem(equalTo(railAPIData.getOriginCode())));
    }

    @And("[rail-search-api] user should see get rail ticket availability response with destination code is same")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithDestinationCodeIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<String> destinationCode = new ArrayList<>();
        switch (railAPIData.getDestinationType()) {
            case "STATION":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    destinationCode.add(departureRailListItem.getDestination().getStationCode());
                break;
            case "CITY":
                for (DepartureRailListItem departureRailListItem : listDepartureRail)
                    destinationCode.add(departureRailListItem.getDestination().getCity().getCityCode());
                break;
            default:
                break;
        }
        assertThat("destination code is different with expected", destinationCode, everyItem(equalTo(railAPIData.getDestinationCode())));
    }

    @And("[rail-search-api] user should see get rail ticket availability response with departure date is same")
    public void railSearchApiUserShouldSeeGetRailTicketAvailabilityResponseWithDepartureDateIsSame() {
        List<DepartureRailListItem> listDepartureRail = railAPIData.getResponseGetRailAvailability().getValue().getDepartureRailList();
        List<String> departureDate = new ArrayList<>();
        for (DepartureRailListItem departureRailListItem : listDepartureRail)
            departureDate.add(departureRailListItem.getDepartureDate());
        assertThat("departure date is different with expected", departureDate, everyItem(equalTo(railAPIData.getDepartureDate())));
    }
}
