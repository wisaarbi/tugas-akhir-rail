package org.example.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;
import org.example.data.RailData;
import org.example.pages.desktop.DesktopRailListPage;
import org.example.properties.RailProperties;
import org.example.utility.DateUtility;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RailListTicketSteps extends ScenarioSteps implements BaseSteps {

    @Autowired
    RailData railData;

    @Autowired
    DesktopRailListPage desktopRailListPage;

    @Autowired
    RailProperties railProperties;

    @Autowired
    DateUtility dateUtility;

    @When("[desktop-rail-list-page] user filter departure time")
    public void desktopRailListPageUserFilterDepartureTime() {
        List<String> listFilterTimeDeparture = desktopRailListPage.getListFilterTimeDeparture();
        String timeDeparture = listFilterTimeDeparture.get(0)
                .substring(0, listFilterTimeDeparture.get(0).indexOf("(") - 1);
        railData.setTimeDeparture(timeDeparture);
        desktopRailListPage.filterDepartureTime(railData.getTimeDeparture());
    }

    @Then("[desktop-rail-list-page] user should see filtered list departure time")
    public void desktopRailListPageUserShouldSeeFilteredListDepartureTime() {
        String lowerTime = null;
        String upperTime = null;
        switch (railData.getTimeDeparture()) {
            case "Pagi":
                lowerTime = "00:00";
                upperTime = "11:59";
                break;
            case "Siang":
                lowerTime = "12:00";
                upperTime = "17:59";
                break;
            case "Malam":
                lowerTime = "18:00";
                upperTime = "23:59";
                break;
            default:
                break;
        }
        assertThat("departure time is different with expected", desktopRailListPage.getListDepartureTime(),
                everyItem(both(greaterThanOrEqualTo(lowerTime)).and(lessThanOrEqualTo(upperTime))));
    }

    @When("[desktop-rail-list-page] user filter departure class")
    public void desktopRailListPageUserFilterDepartureClass() {
        List<String> listFilterClassDeparture = desktopRailListPage.getListFilterClassDeparture();
        railData.setClassDeparture(listFilterClassDeparture.get(0));
        desktopRailListPage.filterDepartureClass(railData.getClassDeparture());
    }

    @Then("[desktop-rail-list-page] user should see filtered list departure class")
    public void desktopRailListPageUserShouldSeeFilteredListDepartureClass() {
        assertThat("departure class is different with expected", desktopRailListPage.getListDepartureClass(),
                everyItem(containsString(railData.getClassDeparture())));
    }

    @When("[desktop-rail-list-page] user filter departure price to {string} price")
    public void desktopRailListPageUserFilterDeparturePriceToPrice(String price) {
        switch (price) {
            case "MAX":
                railData.setMaxPriceDeparture(desktopRailListPage.getMaxPriceDeparture());
                desktopRailListPage.filterDeparturePriceToMaxPrice();
                break;
            case "MIN":
                railData.setMinPriceDeparture(desktopRailListPage.getMinPriceDeparture());
                desktopRailListPage.filterDeparturePriceToMinPrice();
                break;
            default:
                break;
        }

    }

    @Then("[desktop-rail-list-page] user should see filtered list {string} departure price")
    public void desktopRailListPageUserShouldSeeFilteredListDeparturePrice(String price) {
        String expectedPrice = null;
        switch (price) {
            case "MAX":
                expectedPrice = railData.getMaxPriceDeparture();
                break;
            case "MIN":
                expectedPrice = railData.getMinPriceDeparture();
                break;
            default:
                break;
        }
        assertThat("departure price is different with expected", desktopRailListPage.getListDeparturePrice(),
                everyItem(containsString(expectedPrice)));
    }

    @When("[desktop-rail-list-page] user filter departure train type")
    public void desktopRailListPageUserFilterDepartureTrainType() {
        desktopRailListPage.clickDepartureTrainType();
        List<String> listFilterTrainTypeDeparture = desktopRailListPage.getListFilterTrainTypeDeparture();
        railData.setTrainTypeDeparture(listFilterTrainTypeDeparture.get(0));
        desktopRailListPage.filterDepartureTrainType(railData.getTrainTypeDeparture());
    }

    @Then("[desktop-rail-list-page] user should see filtered list departure train type")
    public void desktopRailListPageUserShouldSeeFilteredListDepartureTrainType() {
        assertThat("departure train type is different with expected", desktopRailListPage.getListDepartureTrainType(),
                everyItem(containsString(railData.getTrainTypeDeparture())));
    }

    @When("[desktop-rail-list-page] user click ganti pencarian")
    public void desktopRailListPageUserClickGantiPencarian() {
        desktopRailListPage.clickGantiPencarian();
    }

    @And("[desktop-rail-list-page] user click departure location")
    public void desktopRailListPageUserClickDepartureLocation() {
        desktopRailListPage.clickDepartureLocation();
    }

    @And("[desktop-rail-list-page] user change departure city {string}")
    public void desktopRailListPageUserChangeDepartureCity(String changeDeptCity) {
        switch (changeDeptCity) {
            case "validChangeDeptCity":
                railData.setDeptCity(railProperties.get("validChangeDeptCity"));
                break;
            default:
                railData.setDeptCity(changeDeptCity);
                break;
        }
        desktopRailListPage.changeDepartureCity(railData.getDeptCity());
    }

    @And("[desktop-rail-list-page] user change departure station {string}")
    public void desktopRailListPageUserChangeDepartureStation(String changeDeptStation) {
        switch (changeDeptStation) {
            case "validChangeDeptStation":
                railData.setDeptStation(railProperties.get("validChangeDeptStation"));
                break;
            default:
                railData.setDeptStation(changeDeptStation);
                break;
        }
        desktopRailListPage.changeDepartureStation(railData.getDeptStation());
    }

    @And("[desktop-rail-list-page] user click destination location")
    public void desktopRailListPageUserClickDestinationLocation() {
        desktopRailListPage.clickDestinationLocation();
    }

    @And("[desktop-rail-list-page] user change destination city {string}")
    public void desktopRailListPageUserChangeDestinationCity(String changeDestCity) {
        switch (changeDestCity) {
            case "validChangeDestCity":
                railData.setDestCity(railProperties.get("validChangeDestCity"));
                break;
            default:
                railData.setDestCity(changeDestCity);
                break;
        }
        desktopRailListPage.changeDestinationCity(railData.getDestCity());
    }

    @And("[desktop-rail-list-page] user change destination station {string}")
    public void desktopRailListPageUserChangeDestinationStation(String changeDestStation) {
        switch (changeDestStation) {
            case "validChangeDestStation":
                railData.setDestStation(railProperties.get("validChangeDestStation"));
                break;
            default:
                railData.setDestStation(changeDestStation);
                break;
        }
        desktopRailListPage.changeDestinationStation(railData.getDestStation());
    }

    @And("[desktop-rail-list-page] user change passenger")
    public void desktopRailListPageUserChangePassenger(DataTable passenger) {
        List<Map<String, String>> listTotalPassenger = passenger.asMaps(String.class, String.class);
        String changeTotalAdultPassenger = listTotalPassenger.get(0).get("changeTotalAdultPassenger");
        String changeTotalInfantPassenger = listTotalPassenger.get(0).get("changeTotalInfantPassenger");
        switch (changeTotalAdultPassenger) {
            case "validChangeTotalAdultPassenger":
                railData.setTotalAdultPassenger(Integer.parseInt(railProperties.get("validChangeTotalAdultPassenger")));
                break;
            default:
                railData.setTotalAdultPassenger(Integer.parseInt(changeTotalAdultPassenger));
                break;
        }
        switch (changeTotalInfantPassenger) {
            case "validChangeTotalInfantPassenger":
                railData.setTotalInfantPassenger(Integer.parseInt(railProperties.get("validChangeTotalInfantPassenger")));
                break;
            default:
                railData.setTotalInfantPassenger(Integer.parseInt(changeTotalInfantPassenger));
                break;
        }
        desktopRailListPage.clickPassenger()
                .changePassenger(railData.getTotalAdultPassenger(), railData.getTotalInfantPassenger());
    }

    @And("[desktop-rail-list-page] user click cari")
    public void desktopRailListPageUserClickCari() {
        desktopRailListPage.clickCari();
    }

    @When("[desktop-rail-list-page] user click choose departure ticket")
    public void desktopRailListPageUserClickChooseDepartureTicket() {
        desktopRailListPage.clickChooseDepartureTicket();
    }

    @When("[desktop-rail-list-page] user click choose return ticket")
    public void desktopRailListPageUserClickChooseReturnTicket() {
        desktopRailListPage.clickChooseReturnTicket();
    }

    @Then("[desktop-rail-list-page] user should see ticket summary departure")
    public void desktopRailListPageUserShouldSeeTicketSummaryDeparture() {
        assertThat("ticket summary departure is not visible", desktopRailListPage.isTicketSummaryDepartureVisible(),
                equalTo(true));
    }

    @Then("[desktop-rail-list-page] user should see ticket summary return")
    public void desktopRailListPageUserShouldSeeTicketSummaryReturn() {
        assertThat("ticket summary return is not visible", desktopRailListPage.isTicketSummaryReturnVisible(),
                equalTo(true));
    }

    @When("[desktop-rail-list-page] user filter return time")
    public void desktopRailListPageUserFilterReturnTime() {
        List<String> listFilterTimeReturn = desktopRailListPage.getListFilterTimeReturn();
        String timeReturn = listFilterTimeReturn.get(0)
                .substring(0, listFilterTimeReturn.get(0).indexOf("(") - 1);
        railData.setTimeReturn(timeReturn);
        desktopRailListPage.filterReturnTime(railData.getTimeReturn());
    }

    @Then("[desktop-rail-list-page] user should see filtered list return time")
    public void desktopRailListPageUserShouldSeeFilteredListReturnTime() {
        String lowerTime = null;
        String upperTime = null;
        switch (railData.getTimeReturn()) {
            case "Pagi":
                lowerTime = "00:00";
                upperTime = "11:59";
                break;
            case "Siang":
                lowerTime = "12:00";
                upperTime = "17:59";
                break;
            case "Malam":
                lowerTime = "18:00";
                upperTime = "23:59";
                break;
            default:
                break;
        }
        assertThat("return time is different with expected", desktopRailListPage.getListReturnTime(),
                everyItem(both(greaterThanOrEqualTo(lowerTime)).and(lessThanOrEqualTo(upperTime))));
    }

    @When("[desktop-rail-list-page] user filter return class")
    public void desktopRailListPageUserFilterReturnClass() {
        List<String> listFilterClassReturn = desktopRailListPage.getListFilterClassReturn();
        railData.setClassReturn(listFilterClassReturn.get(0));
        desktopRailListPage.filterReturnClass(railData.getClassReturn());
    }

    @Then("[desktop-rail-list-page] user should see filtered list return class")
    public void desktopRailListPageUserShouldSeeFilteredListReturnClass() {
        assertThat("return class is different with expected", desktopRailListPage.getListReturnClass(),
                everyItem(containsString(railData.getClassReturn())));
    }

    @When("[desktop-rail-list-page] user filter return train type")
    public void desktopRailListPageUserFilterReturnTrainType() {
        desktopRailListPage.clickReturnTrainType();
        List<String> listFilterTrainTypeReturn = desktopRailListPage.getListFilterTrainTypeReturn();
        railData.setTrainTypeReturn(listFilterTrainTypeReturn.get(0));
        desktopRailListPage.filterReturnTrainType(railData.getTrainTypeReturn());
    }

    @Then("[desktop-rail-list-page] user should see filtered list return train type")
    public void desktopRailListPageUserShouldSeeFilteredListReturnTrainType() {
        assertThat("return train type is different with expected", desktopRailListPage.getListReturnTrainType(),
                everyItem(containsString(railData.getTrainTypeReturn())));
    }

    @When("[desktop-rail-list-page] user click order")
    public void desktopRailListPageUserClickOrder() {
        desktopRailListPage.clickOrder();
    }

    @And("[desktop-rail-list-page] user change date of departure with day plus {int}")
    public void desktopRailListPageUserChangeDateOfDepartureWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateDeparture(date);
        desktopRailListPage.clickDateDeparture().changeDateDeparture(railData.getDateDeparture());
    }

    @And("[desktop-rail-list-page] user change date of return with day plus {int}")
    public void desktopRailListPageUserChangeDateOfReturnWithDayPlus(int dayPlus) {
        Date currentDatePlusDay = dateUtility.getCurrentDateWithDayPlus(dayPlus);
        String dateFormat = "dd-MM-yyyy";
        String date = dateUtility.convertDateToString(currentDatePlusDay, dateFormat);
        railData.setDateReturn(date);
        desktopRailListPage.clickDateReturn().changeDateReturn(railData.getDateReturn());
    }
}
