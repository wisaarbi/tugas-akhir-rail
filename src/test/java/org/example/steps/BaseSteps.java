package org.example.steps;

import org.example.config.AppConfig;
import net.serenitybdd.junit.spring.integration.SpringIntegrationMethodRule;
import org.junit.Rule;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = AppConfig.class)
@SpringBootTest
public interface BaseSteps {
    @Rule
    SpringIntegrationMethodRule springIntegrationMethodRule = new SpringIntegrationMethodRule();
}
