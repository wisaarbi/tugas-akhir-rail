package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("org.example.pages.desktop.DesktopFavouritesPage")
public class DesktopFavouritesPage extends WebElementHelper {

    @FindBy(xpath = "//div[contains(@class,'favourite-list')]//div[contains(text(),'Kereta')]")
    private WebElementFacade btnKereta;

    public void clickKereta() {
        scrollToElement(btnKereta);
        btnKereta.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible();
        btnKereta.withTimeoutOf(Duration.ofSeconds(7)).waitUntilClickable();
        waitABit(2000);
        btnKereta.click();
    }

}
