package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("org.example.pages.desktop.DesktopRailSeatPage")
public class DesktopRailSeatPage extends WebElementHelper {

    @FindBy(xpath = "//div[@class='seat__info-class']//select")
    private WebElementFacade dropdownCarriage;

    @FindBy(xpath = "//li[contains(@class,'seat__available')]")
    private List<WebElementFacade> availableSeat;

    @FindBy(xpath = "//span[contains(@ng-bind,'passenger.wagonCode')]")
    private List<WebElementFacade> selectedCarriage;

    @FindBy(xpath = "//span[contains(@ng-bind,'passenger.seatRow')]")
    private List<WebElementFacade> selectedSeat;

    @FindBy(xpath = "//button[@class='seat__btn seat__btn--orange']")
    private WebElementFacade btnProceedToPayment;

    @FindBy(xpath = "//label[@class='seat__for-name ng-binding']")
    private List<WebElementFacade> listPassenger;

    @FindBy(xpath = "//button[@class='seat__btn seat__btn--blueline']")
    private WebElementFacade btnSelectReturnSeat;

    public DesktopRailSeatPage chooseCarriage() {
        for(int i = 0; i < getSizeOptions(dropdownCarriage); i++) {
            dropdownCarriage.selectByIndex(i);
            if (isListWebElementFacadeVisible(availableSeat)) {
                break;
            }
        }
        return this;
    }

    public List<String> getSelectedCarriage() {
        List<String> listSelectedCarriage = new ArrayList<>();
        for (WebElementFacade webElementFacade : selectedCarriage) {
            listSelectedCarriage.add(webElementFacade.getText());
        }
        return listSelectedCarriage;
    }

    public DesktopRailSeatPage chooseSeat() {
        availableSeat.get(0).click();
        return this;
    }

    public List<String> getSelectedSeat() {
        List<String> listSelectedSeat = new ArrayList<>();
        for (WebElementFacade webElementFacade : selectedSeat) {
            listSelectedSeat.add(webElementFacade.getText());
        }
        return listSelectedSeat;
    }

    public void clickProceedToPayment() {
        btnProceedToPayment.click();
    }

    public DesktopRailSeatPage choosePassenger(int indexPassenger) {
        listPassenger.get(indexPassenger).click();
        return this;
    }

    public DesktopRailSeatPage clickSelectReturnSeat() {
        btnSelectReturnSeat.click();
        return this;
    }
}
