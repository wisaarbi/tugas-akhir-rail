package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("org.example.pages.desktop.DesktopProfilePage")
@DefaultUrl("https://blibli.com/member/profile")
public class DesktopProfilePage extends WebElementHelper {

    @FindBy(xpath = "//label[contains(text(),'Email akun')]/preceding-sibling::input")
    private WebElementFacade txtFieldEmail;

    public String getEmail() {
        return txtFieldEmail.withTimeoutOf(Duration.ofSeconds(7))
                .waitUntilVisible().getAttribute("value");
    }
}
