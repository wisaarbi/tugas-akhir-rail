package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

@Component("org.example.pages.desktop.DesktopRailThankYouPage")
public class DesktopRailThankYouPage extends PageObject {

    @FindBy(xpath = "//article[@class='site__main-top']")
    private WebElementFacade thankYouSection;

    public boolean isThankYouSectionVisible() {
        return thankYouSection.isVisible();
    }
}
