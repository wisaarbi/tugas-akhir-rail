package org.example.pages.desktop;

import io.restassured.http.Cookies;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.example.properties.DefaultProperties;
import org.example.utility.RandomUtility;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class WebElementHelper extends PageObject {

    @Autowired
    DefaultProperties defaultProperties;

    @Autowired
    RandomUtility randomUtility;

    public void scrollToElement(WebElementFacade webElementFacade) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView(false);", webElementFacade);
    }

    public Cookies getCookies() {
        Set<Cookie> seleniumCookies = getDriver().manage().getCookies();
        List<io.restassured.http.Cookie> restAssuredCookies = new ArrayList<>();
        for (Cookie cookie : seleniumCookies)
            restAssuredCookies.add(new io.restassured.http.Cookie.Builder(cookie.getName(), cookie.getValue()).build());
        return new Cookies(restAssuredCookies);
    }

    public void addDefaultCookies() {
        HashMap<String, String> cookies = defaultProperties.getCookies();
        for (Map.Entry<String, String> entry : cookies.entrySet()) {
            Cookie cookie = new Cookie(entry.getKey(), entry.getValue());
            getDriver().manage().addCookie(cookie);
        }
    }

    public String getSelectedOption(WebElementFacade webElementFacade) {
        Select select = new Select(webElementFacade);
        return select.getFirstSelectedOption().getText();
    }

    public int getSizeOptions(WebElementFacade webElementFacade) {
        Select select = new Select(webElementFacade);
        return select.getOptions().size();
    }

    public boolean isListWebElementFacadeVisible(List<WebElementFacade> listWebElementFacade) {
        for (WebElementFacade webElementFacade : listWebElementFacade) {
            if (!webElementFacade.isVisible()) {
                return false;
            }
        }
        return true;
    }

    public void scrollPage(int pixel) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollBy(0," + pixel + ")");
    }

    public void openNewTab() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.open()");
    }

    public void takeControlOfTheNewTab() {
        ArrayList<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
    }

    public void closeTheNewTab() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.close()");
    }

    public void takeControlOfTheOldTab() {
        ArrayList<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(0));
    }
}
