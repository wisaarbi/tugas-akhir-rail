package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("org.example.pages.desktop.DesktopHomePage")
@DefaultUrl("https://blibli.com/")
public class DesktopHomePage extends WebElementHelper {

    @FindBy(xpath = "//div[@class='favourite__item']//div[contains(text(),'Semua')]")
    private WebElementFacade btnAllCategory;

    @FindBy(xpath = "//iframe[contains(@class,'sp-fancybox-iframe sp-fancybox-skin sp-fancybox-iframe')]")
    private WebElementFacade iframe;

    @FindBy(xpath = "//div[@class='element-content'][contains(@id,'close-button')]")
    private WebElementFacade btnCloseIframe;

    @FindBy(xpath = "//div[@class='blu-modal__container']//button[contains(@class,'decline')]")
    private WebElementFacade btnDeclineLocationPermission;

    public void clickAllCategory() {
        scrollToElement(btnAllCategory);
        btnAllCategory.click();
    }

    public DesktopHomePage closeIframeIfShowUp() {
        getDriver().switchTo().frame(iframe);
        btnCloseIframe.click();
        getDriver().switchTo().defaultContent();
        return this;
    }

    public boolean isIframeVisible() {
        return iframe.isVisible();
    }

    public void declineLocationPermission() {
        btnDeclineLocationPermission.withTimeoutOf(Duration.ofSeconds(7))
                .waitUntilVisible().click();
    }
}
