package org.example.pages.desktop;

import lombok.SneakyThrows;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.text.WordUtils;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

@Component("org.example.pages.desktop.DesktopRailSearchPage")
public class DesktopRailSearchPage extends WebElementHelper {

    @FindBy(xpath = "//form[@id='rail-search']")
    private WebElementFacade formRailSearch;

    @FindBy(xpath = "//div[@class='rail-search__container-dest--from']//input")
    private WebElementFacade inputLocationDeparture;

    @FindBy(xpath = "//div[@class='rail-search__container-dest--to']//input")
    private WebElementFacade inputLocationDestination;

    private static final String xpathDeptStation = "//div[@class='rail-search__container-dest--from']//p[@data-type='station'][@data-name='%s']/ancestor::li";

    private static final String xpathDestStation = "//div[@class='rail-search__container-dest--from']//p[@data-type='station'][@data-name='%s']/ancestor::li";

    private static final String xpathListCurrentDateTableDeparture = "(//table[@class='month1'])[1]//div[contains(@class,'day toMonth  valid')]";

    private static final String xpathListCurrentDateTableReturn = "(//table[@class='month1'])[2]//div[contains(@class,'day toMonth  valid')]";

    @FindBy(xpath = "//input[@id='search__dep-date']")
    private WebElementFacade btnDateDeparture;

    @FindBy(xpath = "//label[@for='train-return']")
    private WebElementFacade btnDateReturn;

    @FindBy(xpath = "(//table[@class='month2'])[1]//span[@class='next datepicker__next']")
    private WebElementFacade btnNextMonthDeparture;

    @FindBy(xpath = "(//table[@class='month1'])[1]//th[@class='month-name']")
    private WebElementFacade currentMonthTableDeparture;

    @FindBy(xpath = "(//table[@class='month2'])[2]//span[@class='next datepicker__next']")
    private WebElementFacade btnNextMonthReturn;

    @FindBy(xpath = "(//table[@class='month1'])[2]//th[@class='month-name']")
    private WebElementFacade currentMonthTableReturn;

    @FindBy(xpath = "//div[@class='rail-search__container-passenger']//input")
    private WebElementFacade btnPassenger;

    @FindBy(xpath = "//div[@class='passenger__value']//span[contains(@class,'adult')]")
    private WebElementFacade currentTotalAdultPassenger;

    @FindBy(xpath = "//div[@class='passenger__value']//span[contains(@class,'infant')]")
    private WebElementFacade currentTotalInfantPassenger;

    @FindBy(xpath = "(//a[@class='passenger__inc'])[1]")
    private WebElementFacade btnMinusAdultPassenger;

    @FindBy(xpath = "(//a[@class='passenger__desc'])[1]")
    private WebElementFacade btnPlusAdultPassenger;

    @FindBy(xpath = "(//a[@class='passenger__inc'])[2]")
    private WebElementFacade btnMinusInfantPassenger;

    @FindBy(xpath = "(//a[@class='passenger__desc'])[2]")
    private WebElementFacade btnPlusInfantPassenger;

    @FindBy(xpath = "//button[@class='rail-search__btn']")
    private WebElementFacade btnCari;

    @FindBy(xpath = "//div[@id='sweet-alert-id']//h2")
    private WebElementFacade emptyStationErrorMessage;

    @FindBy(xpath = "//div[@class='rail-search__container-dest--from']//div[@id='rail-search__ac-suggest']")
    private WebElementFacade listPopularCity;

    @FindBy(xpath = "//div[@class='passenger__block-items passenger__block-items--error']")
    private WebElementFacade totalPassengerErrorMessage;

    @FindBy(xpath = "//p[@class='rail-search__ac-error rail-search__ac-no-match']")
    private WebElementFacade searchErrorMessage;

    @FindBy(xpath = "//div[contains(@class,'ins-coupon-content-opened')]")
    private WebElementFacade couponContent;

    public boolean isRailSearchFormVisible() {
        return formRailSearch.isVisible();
    }

    public DesktopRailSearchPage fillDepartureCity(String deptCity) {
        inputLocationDeparture.type(deptCity);
        return this;
    }

    public DesktopRailSearchPage fillDepartureStation(String deptStation) {
        waitABit(3000);
        String formattedDeptStation = WordUtils.capitalizeFully(deptStation);
        WebElementFacade departureStation = find(By.xpath(String.format(xpathDeptStation, formattedDeptStation)));
        if(departureStation.isVisible()) {
            departureStation.click();
        }
        return this;
    }

    public DesktopRailSearchPage fillDestinationCity(String destCity) {
        inputLocationDestination.type(destCity);
        return this;
    }

    public DesktopRailSearchPage fillDestinationStation(String destStation) {
        waitABit(3000);
        String formattedDestStation = WordUtils.capitalizeFully(destStation);
        WebElementFacade destinationStation = find(By.xpath(String.format(xpathDestStation, formattedDestStation)));
        if(destinationStation.isVisible()) {
            destinationStation.click();
        }
        return this;
    }

    public DesktopRailSearchPage clickDateDeparture() {
        btnDateDeparture.click();
        return this;
    }

    @SneakyThrows
    public DesktopRailSearchPage fillDateDeparture(String dateDeparture) {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateDeparture = LocalDate.parse(dateDeparture, formatter);
        while (!currentMonthTableDeparture.getText().contains(localDateDeparture.getMonth().getDisplayName(TextStyle.FULL, locale))) {
            btnNextMonthDeparture.click();
        }
        List<WebElementFacade> listCurrentDateTableDeparture = findAll(xpathListCurrentDateTableDeparture);
        for (WebElementFacade webElementFacade : listCurrentDateTableDeparture) {
            if (webElementFacade.getText().equals(String.valueOf(localDateDeparture.getDayOfMonth()))) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public DesktopRailSearchPage clickDateReturn() {
        btnDateReturn.click();
        return this;
    }

    public DesktopRailSearchPage fillDateReturn(String dateReturn) {
        Locale locale = new Locale("in", "ID");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", locale);
        LocalDate localDateReturn = LocalDate.parse(dateReturn, formatter);
        while (!currentMonthTableReturn.getText().contains(localDateReturn.getMonth().getDisplayName(TextStyle.FULL, locale))) {
            btnNextMonthReturn.click();
        }
        List<WebElementFacade> listCurrentDateTableReturn = findAll(xpathListCurrentDateTableReturn);
        for (WebElementFacade webElementFacade : listCurrentDateTableReturn) {
            if (webElementFacade.getText().equals(String.valueOf(localDateReturn.getDayOfMonth()))) {
                webElementFacade.click();
                break;
            }
        }
        return this;
    }

    public DesktopRailSearchPage clickPassenger() {
        btnPassenger.click();
        return this;
    }

    public DesktopRailSearchPage fillPassenger(int totalAdultPassenger, int totalInfantPassenger) {
        fillAdultPassenger(totalAdultPassenger);
        fillInfantPassenger(totalInfantPassenger);
        return this;
    }

    private void fillInfantPassenger(int totalInfantPassenger) {
        int totalInfant = Integer.parseInt(currentTotalInfantPassenger.getText());
        while (totalInfant != totalInfantPassenger) {
            if(totalInfant < totalInfantPassenger) {
                btnPlusInfantPassenger.click();
                totalInfant++;
            }
            else {
                btnMinusInfantPassenger.click();
                totalInfant--;
            }
        }
    }

    private void fillAdultPassenger(int totalAdultPassenger) {
        int totalAdult = Integer.parseInt(currentTotalAdultPassenger.getText());
        while (totalAdult != totalAdultPassenger) {
            if(totalAdult < totalAdultPassenger) {
                btnPlusAdultPassenger.click();
                totalAdult++;
            }
            else {
                btnMinusAdultPassenger.click();
                totalAdult--;
            }
        }
    }

    public void clickCari() {
        btnCari.click();
    }

    public boolean isEmptyStationErrorMessageVisible() {
        return emptyStationErrorMessage.isVisible();
    }

    public String getEmptyStationErrorMessage() {
        return emptyStationErrorMessage.getText();
    }

    public DesktopRailSearchPage clickDepartureLocation() {
        inputLocationDeparture.click();
        return this;
    }

    public boolean isListPopularCityVisible() {
        return listPopularCity.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().isVisible();
    }

    public DesktopRailSearchPage clickDestinationLocation() {
        inputLocationDestination.click();
        return this;
    }

    public String getTotalPassengerErrorMessage() {
        return totalPassengerErrorMessage.getText();
    }

    public boolean isTotalPassengerErrorMessageVisible() {
        return totalPassengerErrorMessage.isVisible();
    }

    public boolean isSearchErrorMessageVisible() {
        return searchErrorMessage.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().isVisible();
    }
}
