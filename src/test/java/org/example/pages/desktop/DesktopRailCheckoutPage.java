package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component("org.example.pages.desktop.DesktopRailCheckoutPage")
public class DesktopRailCheckoutPage extends WebElementHelper {

    @FindBy(xpath = "//section[@class='order-detail']")
    private WebElementFacade sectionOrderDetail;

    @FindBy(xpath = "//div[@ng-bind='cartData.tripInformation.departureOriginStationName']")
    private WebElementFacade deptOriginStation;

    @FindBy(xpath = "//div[@ng-bind='cartData.tripInformation.departureDestinationStationName']")
    private WebElementFacade deptDestinationStation;

    @FindBy(xpath = "//div[@ng-bind='cartData.tripInformation.returnOriginStationName']")
    private WebElementFacade returnOriginStation;

    @FindBy(xpath = "//div[@ng-bind='cartData.tripInformation.returnDestinationStationName']")
    private WebElementFacade returnDestinationStation;

    @FindBy(xpath = "//div[@ng-bind='cartData.departureFareDisplay']")
    private WebElementFacade deptTicketPrice;

    @FindBy(xpath = "//div[@ng-bind='cartData.returnFareDisplay']")
    private WebElementFacade returnTicketPrice;

    @FindBy(xpath = "//div[@ng-bind='cartData.totalOrderDisplay']")
    private WebElementFacade totalTicketPrice;

    @FindBy(xpath = "//div[contains(@class,'order-detail__price-label--total')]")
    private WebElementFacade totalPassenger;

    @FindBy(xpath = "//span[@ng-bind='cartData.tripInformation.departureTrainClassDisplay']")
    private WebElementFacade deptTrainClass;

    @FindBy(xpath = "//span[@ng-bind='cartData.tripInformation.returnTrainClassDisplay']")
    private WebElementFacade returnTrainClass;

    @FindBy(xpath = "//span[@ng-bind='cartData.tripInformation.departureTrainName']")
    private WebElementFacade deptTrainType;

    @FindBy(xpath = "//span[@ng-bind='cartData.tripInformation.returnTrainName']")
    private WebElementFacade returnTrainType;

    @FindBy(xpath = "//span[@ng-bind='cartData.tripInformation.departureDateDisplay']")
    private WebElementFacade deptDate;

    @FindBy(xpath = "//span[@ng-bind='cartData.tripInformation.returnDateDisplay']")
    private WebElementFacade returnDate;

    @FindBy(xpath = "//select[@ng-model='contactData.title']")
    private WebElementFacade dropdownCustomerTitle;

    @FindBy(xpath = "//input[@ng-model='contactData.fullName']")
    private WebElementFacade txtFieldCustomerName;

    @FindBy(xpath = "//input[@ng-model='contactData.phone']")
    private WebElementFacade txtFieldCustomerPhoneNumber;

    @FindBy(xpath = "//input[@ng-model='contactData.email']")
    private WebElementFacade txtFieldCustomerEmail;

    @FindBy(xpath = "//section[contains(@class,'checkout__form--buyer show')]//a[contains(@class,'checkout__btn')]")
    private WebElementFacade btnContinueToFillInPassengerData;

    @FindBy(id = "sweet-alert-confirm")
    private WebElementFacade btnAcceptTNCCOVID;

    @FindBy(xpath = "//label[@ng-bind='contactData.title.label']")
    private WebElementFacade customerTitle;

    @FindBy(xpath = "//label[@ng-bind='contactData.fullName']")
    private WebElementFacade customerName;

    @FindBy(xpath = "//div[@ng-bind='contactData.phone']")
    private WebElementFacade customerPhoneNumber;

    @FindBy(xpath = "//div[@ng-bind='contactData.email']")
    private WebElementFacade customerEmail;

    @FindBy(xpath = "//section[contains(@class,'checkout__form--buyer show')]//span[@class='error__msg ng-binding']")
    private WebElementFacade errorMessageOnCustomerSection;

    @FindBy(xpath = "(//label[@for='same-as-buyer'])[1]")
    private WebElementFacade btnSameAsCustomer;

    @FindBy(xpath = "//a[@class='checkout__action-btn-left']")
    private WebElementFacade btnChangeSeat;

    private static final String xpathDropdownAdultPassengerTitle = "(//div[contains(@ng-repeat,'adultForm')]//select[contains(@class,'checkout__select--title')])[%s]";

    private static final String xpathTxtFieldAdultPassengerName = "(//div[contains(@ng-repeat,'adultForm')]//span[contains(text(),'Nama Lengkap')]/ancestor::div[@class='checkout__input-wrapper']//input)[%s]";

    private static final String xpathTxtFieldAdultIdentityCard = "(//div[contains(@ng-repeat,'adultForm')]//span[contains(text(),'KTP')]/ancestor::div[@class='checkout__input-wrapper']//input)[%s]";

    private static final String xpathBtnGoToNextAdultPassenger = "(//div[contains(@ng-repeat,'adultForm')]//div[contains(@ng-click,'goToNextPassenger')])[%s]";

    private static final String xpathDropdownInfantPassengerTitle = "(//div[contains(@ng-repeat,'infantForm')]//select[contains(@class,'checkout__select--title')])[%s]";

    private static final String xpathTxtFieldInfantPassengerName = "(//div[contains(@ng-repeat,'infantForm')]//span[contains(text(),'Nama Lengkap')]/ancestor::div[@class='checkout__input-wrapper']//input)[%s]";

    private static final String xpathTxtFieldInfantIdentityCard = "(//div[contains(@ng-repeat,'infantForm')]//span[contains(text(),'KTP')]/ancestor::div[@class='checkout__input-wrapper']//input)[%s]";

    private static final String xpathBtnGoToNextInfantPassenger = "(//div[contains(@ng-repeat,'infantForm')]//div[contains(@ng-click,'goToNextPassenger')])[%s]";

    @FindBy(xpath = "//div[@class='checkout__box box ng-scope box--done'][contains(@ng-repeat,'adultForm')]")
    private List<WebElementFacade> adultPassengerData;

    @FindBy(xpath = "//div[@class='checkout__box box ng-scope box--done'][contains(@ng-repeat,'infantForm')]")
    private List<WebElementFacade> infantPassengerData;

    @FindBy(xpath = "//a[@class='checkout__action-btn-right']")
    private WebElementFacade btnProceedToPayment;

    public boolean isOrderDetailVisible() {
        return sectionOrderDetail.isVisible();
    }

    public String getDepartureOriginStation() {
        return deptOriginStation.getText();
    }

    public String getDepartureDestinationStation() {
        return deptDestinationStation.getText();
    }

    public String getReturnOriginStation() {
        return returnOriginStation.getText();
    }

    public String getReturnDestinationStation() {
        return returnDestinationStation.getText();
    }

    public String getTotalPassenger() {
        return totalPassenger.getText();
    }

    public boolean isDepartureTicketPriceVisible() {
        return deptTicketPrice.isVisible();
    }

    public boolean isReturnTicketPriceVisible() {
        return returnTicketPrice.isVisible();
    }

    public boolean isTotalTicketPriceVisible() {
        return totalTicketPrice.isVisible();
    }

    public String getDepartureTrainClass() {
        return deptTrainClass.getText();
    }

    public String getReturnTrainClass() {
        return returnTrainClass.getText();
    }

    public String getDepartureTrainType() {
        return deptTrainType.getText();
    }

    public String getReturnTrainType() {
        return returnTrainType.getText();
    }

    public String getDepartureDate() {
        return deptDate.getText();
    }

    public String getReturnDate() {
        return returnDate.getText();
    }

    public DesktopRailCheckoutPage fillCustomerTitle(String customerTitle) {
        if (customerTitle != null && !customerTitle.equals("")) {
            dropdownCustomerTitle.selectByVisibleText(customerTitle);
        }
        return this;
    }

    public DesktopRailCheckoutPage fillCustomerName(String customerName) {
        txtFieldCustomerName.type(customerName);
        return this;
    }

    public DesktopRailCheckoutPage fillCustomerPhoneNumber(String customerPhoneNumber) {
        txtFieldCustomerPhoneNumber.type(customerPhoneNumber);
        return this;
    }

    public DesktopRailCheckoutPage fillCustomerEmail(String customerEmail) {
        txtFieldCustomerEmail.type(customerEmail);
        return this;
    }

    public DesktopRailCheckoutPage clickContinueToFillInPassengerData() {
        btnContinueToFillInPassengerData.click();
        return this;
    }

    public DesktopRailCheckoutPage clickAcceptTNCCOVID() {
        btnAcceptTNCCOVID.withTimeoutOf(Duration.ofSeconds(5)).waitUntilVisible().click();
        return this;
    }

    public String getCustomerTitle() {
        return customerTitle.getText();
    }

    public String getCustomerName() {
        return customerName.getText();
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber.getText();
    }

    public String getCustomerEmail() {
        return customerEmail.getText();
    }

    public String getErrorMessageOnCustomerSection() {
        return errorMessageOnCustomerSection.getText();
    }

    public DesktopRailCheckoutPage clickSameAsCustomer() {
        btnSameAsCustomer.click();
        return this;
    }

    public String getTitlePassengerSameAsCustomer() {
        WebElementFacade webElementFacade = find(By.xpath(String.format(xpathDropdownAdultPassengerTitle, 1)));
        return getSelectedOption(webElementFacade);
    }

    public String getNamePassengerSameAsCustomer() {
        WebElementFacade webElementFacade = find(By.xpath(String.format(xpathTxtFieldAdultPassengerName, 1)));
        return webElementFacade.getAttribute("value");
    }

    public DesktopRailCheckoutPage fillAdultPassenger(int totalAdultPassenger) {
        for (int i = 0; i < totalAdultPassenger; i++) {
            fillAdultPassengerTitleWithRandomTitle(i+1);
            fillAdultPassengerNameWithRandomName(i+1);
            fillAdultPassengerIdentityCardWithRandomIdentityCard(i+1);
            clickBtnGoToNextAdultPassenger(i+1);
        }
        return this;
    }

    private void clickBtnGoToNextAdultPassenger(int index) {
        WebElementFacade btnGoToNextAdultPassenger = find(By.xpath(String.format(xpathBtnGoToNextAdultPassenger, index)));
        btnGoToNextAdultPassenger.click();
    }

    private void fillAdultPassengerIdentityCardWithRandomIdentityCard(int index) {
        String adultPassengerIdentityCard = randomUtility.generateRandomDigitNumber(8);
        WebElementFacade txtFieldAdultPassengerIdentityCard = find(By.xpath(String.format(xpathTxtFieldAdultIdentityCard, index)));
        txtFieldAdultPassengerIdentityCard.type(adultPassengerIdentityCard);
    }

    private void fillAdultPassengerNameWithRandomName(int index) {
        String adultPassengerName = RandomStringUtils.randomAlphabetic(8);
        WebElementFacade txtFieldAdultPassengerName = find(By.xpath(String.format(xpathTxtFieldAdultPassengerName, index)));
        txtFieldAdultPassengerName.type(adultPassengerName);
    }

    private void fillAdultPassengerTitleWithRandomTitle(int index) {
        ArrayList<String> titles = new ArrayList<>(Arrays. asList("Tuan", "Nyonya", "Nona"));
        String adultPassengerTitle = randomUtility.getSingleRandomItemFromList(titles);
        WebElementFacade dropdownAdultPassengerTitle = find(By.xpath(String.format(xpathDropdownAdultPassengerTitle, index)));
        dropdownAdultPassengerTitle.selectByVisibleText(adultPassengerTitle);
    }

    public DesktopRailCheckoutPage fillInfantPassenger(int totalInfantPassenger) {
        for (int i = 0; i < totalInfantPassenger; i++) {
            fillInfantPassengerTitleWithRandomTitle(i+1);
            fillInfantPassengerNameWithRandomName(i+1);
            fillInfantPassengerIdentityCardWithRandomIdentityCard(i+1);
            clickBtnGoToNextInfantPassenger(i+1);
        }
        return this;
    }

    private void clickBtnGoToNextInfantPassenger(int index) {
        WebElementFacade btnGoToNextInfantPassenger = find(By.xpath(String.format(xpathBtnGoToNextInfantPassenger, index)));
        btnGoToNextInfantPassenger.click();
    }

    private void fillInfantPassengerIdentityCardWithRandomIdentityCard(int index) {
        String infantPassengerIdentityCard = randomUtility.generateRandomDigitNumber(8);
        WebElementFacade txtFieldInfantPassengerIdentityCard = find(By.xpath(String.format(xpathTxtFieldInfantIdentityCard, index)));
        txtFieldInfantPassengerIdentityCard.type(infantPassengerIdentityCard);
    }

    private void fillInfantPassengerNameWithRandomName(int index) {
        String infantPassengerName = RandomStringUtils.randomAlphabetic(8);
        WebElementFacade txtFieldInfantPassengerName = find(By.xpath(String.format(xpathTxtFieldInfantPassengerName, index)));
        txtFieldInfantPassengerName.type(infantPassengerName);
    }

    private void fillInfantPassengerTitleWithRandomTitle(int index) {
        ArrayList<String> titles = new ArrayList<>(Arrays. asList("Sdr", "Sdri"));
        String infantPassengerTitle = randomUtility.getSingleRandomItemFromList(titles);
        WebElementFacade dropdownInfantPassengerTitle = find(By.xpath(String.format(xpathDropdownInfantPassengerTitle, index)));
        dropdownInfantPassengerTitle.selectByVisibleText(infantPassengerTitle);
    }

    public boolean isAdultPassengerDataVisible() {
        return isListWebElementFacadeVisible(adultPassengerData);
    }

    public boolean isInfantPassengerDataVisible() {
        return isListWebElementFacadeVisible(infantPassengerData);
    }

    public void clickChangeSeat() {
        btnChangeSeat.click();
    }

    public void clickProceedToPayment() {
        btnProceedToPayment.click();
    }
}
