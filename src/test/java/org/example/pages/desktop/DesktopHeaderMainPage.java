package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("org.example.pages.desktop.DesktopHeaderMainPage")
public class DesktopHeaderMainPage extends WebElementHelper {

    @FindBy(xpath = "//div[@class='account tooltip__trigger']")
    private WebElementFacade accountDetail;

    @FindBy(xpath = "//a[@class='dropdown-member__item dropdown-member__item--profile']")
    private WebElementFacade btnProfile;

    public void clickProfile() {
        accountDetail.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible();
        scrollPage(50);
        withAction().moveToElement(accountDetail).build().perform();
        btnProfile.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().click();
    }
}
