package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("org.example.pages.desktop.DesktopHeaderTravelPage")
public class DesktopHeaderTravelPage extends WebElementHelper {

    @FindBy(id = "gdn-login-link")
    private WebElementFacade btnLogin;

    public void clickLogin() {
        btnLogin.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().click();
    }
}
