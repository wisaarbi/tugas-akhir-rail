package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Component("org.example.pages.desktop.DesktopRailPaymentPage")
public class DesktopRailPaymentPage extends WebElementHelper {

    @FindBy(xpath = "//div[@ng-bind=\"passenger.wagonCode +'-'+passenger.wagonNo+' / '+passenger.seatRow+ passenger.seatColumn\"]")
    private List<WebElementFacade> seatPassengerDeparture;

    @FindBy(xpath = "//div[@ng-bind=\"passenger.wagonCodeRet +'-'+passenger.wagonNoRet+' / '+passenger.seatRowRet+passenger.seatColumnRet\"]")
    private List<WebElementFacade> seatPassengerReturn;

    @FindBy(xpath = "//header[@id='toggle__passenger-summary']")
    private WebElementFacade btnPassengerDetails;

    @FindBy(xpath = "//header[@id='toggle__invoice-summary']")
    private WebElementFacade btnBillDetails;

    @FindBy(xpath = "//header[@id='toggle__trip-summary']")
    private WebElementFacade btnTripDetails;

    @FindBy(xpath = "//button[@id='gdn-payment-list-AlfaGroup']")
    private WebElementFacade btnPaymentMethod;

    @FindBy(xpath = "//button[@id='gdn-payment-list-AlfaGroup']/parent::div//select")
    private WebElementFacade btnPaymentOption;

    private static final String xpathListPaymentOption = "//button[@id='gdn-payment-list-AlfaGroup']/parent::div//select//option";

    @FindBy(xpath = "//button[@id='gdn-payment-list-AlfaGroup']/parent::div//button[@id='gdn-pay-btn']")
    private WebElementFacade btnPayNow;

    @FindBy(xpath = "//button[contains(@class,'cancel-order')]")
    private WebElementFacade btnCancelOrder;

    @FindBy(xpath = "//button[@class='confirm']")
    private WebElementFacade btnConfirmCancelOrder;

    @FindBy(xpath = "//div[@id='sweet-alert-id']//p")
    private WebElementFacade messageCancelOrder;

    @FindBy(xpath = "//div[@class='loading__angular__wrapper']//img[@name='GdnLoaderImage']")
    private WebElementFacade loadingProgress;

    public DesktopRailPaymentPage clickPassengerDetails() {
        scrollToElement(btnPassengerDetails);
        btnPassengerDetails.click();
        return this;
    }

    public List<String> getSeatPassengerDeparture() {
        List<String> listSeatPassengerDeparture = new ArrayList<>();
        for (WebElementFacade webElementFacade : seatPassengerDeparture) {
            listSeatPassengerDeparture.add(webElementFacade.getText());
        }
        return listSeatPassengerDeparture;
    }

    public List<String> getSeatPassengerReturn() {
        List<String> listSeatPassengerReturn = new ArrayList<>();
        for (WebElementFacade webElementFacade : seatPassengerReturn) {
            listSeatPassengerReturn.add(webElementFacade.getText());
        }
        return listSeatPassengerReturn;
    }

    public boolean isBillDetailsVisible() {
        return btnBillDetails.isVisible();
    }

    public boolean isTripDetailsVisible() {
        return btnTripDetails.isVisible();
    }

    public boolean isPassengerDetailsVisible() {
        return btnPassengerDetails.isVisible();
    }

    public void fillPaymentMethod() {
        btnPaymentMethod.click();
        btnPaymentOption.click();
        List<WebElementFacade> listOption = findAll(xpathListPaymentOption);
        listOption.get(1).click();
        loadingProgress.withTimeoutOf(Duration.ofSeconds(7)).waitUntilNotVisible();
        btnPayNow.click();
    }

    public DesktopRailPaymentPage clickCancelOrder() {
        btnCancelOrder.click();
        return this;
    }

    public void clickConfirmCancelOrder() {
        btnConfirmCancelOrder.click();
    }

    public boolean isMessageCancelOrderVisible() {
        return messageCancelOrder.isVisible();
    }
}
