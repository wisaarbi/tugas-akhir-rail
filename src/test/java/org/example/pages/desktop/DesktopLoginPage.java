package org.example.pages.desktop;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("org.example.pages.desktop.DesktopLoginPage")
@DefaultUrl("https://www.blibli.com/login?redirect=%2F")
public class DesktopLoginPage extends WebElementHelper {

    @FindBy(xpath = "//div[@class='blu-card__content']")
    private WebElementFacade loginForm;

    @FindBy(xpath = "//input[@class='form__input login__username']")
    private WebElementFacade txtFieldEmail;

    @FindBy(xpath = "//input[@class='form__input login__password']")
    private WebElementFacade txtFieldPassword;

    @FindBy(xpath = "//div[@class='login__button']//button")
    private WebElementFacade btnLogin;

    @FindBy(xpath = "//div[@class='error']")
    private WebElementFacade errorMessage;

    @FindBy(xpath = "//button[@class='blu-btn otp-validation__button b-full-width b-secondary']")
    private WebElementFacade btnSendVerificationCodeViaEmail;

    @FindBy(xpath = "//input[@class='otp__textField not-active']")
    private WebElementFacade txtFieldVerificationCode;

    @FindBy(xpath = "//div[@class='otp__confirm']//button")
    private WebElementFacade btnVerification;

    public boolean isLoginFormVisible() {
        return loginForm.isVisible();
    }

    public DesktopLoginPage fillEmail(String email) {
        txtFieldEmail.type(email);
        return this;
    }

    public DesktopLoginPage fillPassword(String password) {
        txtFieldPassword.type(password);
        return this;
    }

    public void clickLogin() {
        btnLogin.withTimeoutOf(Duration.ofSeconds(7)).waitUntilVisible().click();
    }

    public boolean isErrorMessageVisible() {
        return errorMessage.isVisible();
    }

    public String getErrorMessage() {
        return errorMessage.getText();
    }

    public boolean isButtonLoginEnabled() {
        return btnLogin.isEnabled();
    }

    public DesktopLoginPage clickVerificationCodeViaEmail() {
        btnSendVerificationCodeViaEmail.click();
        return this;
    }

    public DesktopLoginPage fillVerificationCode(String verificationCode) {
        txtFieldVerificationCode.type(verificationCode);
        return this;
    }

    public void clickVerification() {
        btnVerification.click();
        waitABit(1000);
    }
}
