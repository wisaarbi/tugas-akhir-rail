package org.example.data;

import io.restassured.http.Cookies;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component("org.example.data.LoginData")
@Data
public class LoginData {
    private String email;
    private String password;
    private String verificationCode;

    private Cookies loginCookies;
}
