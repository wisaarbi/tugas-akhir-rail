package org.example.data;

import lombok.Data;
import org.example.responses.checkholiday.getcheckholiday.GetCheckHolidayResponse;
import org.springframework.stereotype.Component;

@Component("org.example.data.CheckHolidayAPIData")
@Data
public class CheckHolidayAPIData {
    private int yearStart;
    private int monthStart;
    private int yearEnd;
    private int monthEnd;

    private GetCheckHolidayResponse responseGetCheckHoliday;
}
