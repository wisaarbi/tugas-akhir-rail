package org.example.data;

import lombok.Data;
import org.example.responses.railavailability.getrailticketavailability.GetRailTicketAvailabilityResponse;
import org.example.responses.railavailability.getsearchpopularstation.GetSearchPopularStationResponse;
import org.springframework.stereotype.Component;

@Component("org.example.data.RailAPIData")
@Data
public class RailAPIData {
    private String originCode;
    private String originType;
    private String destinationCode;
    private String destinationType;
    private String departureDate;
    private String returnDate;
    private int adult;
    private int infant;

    private GetRailTicketAvailabilityResponse responseGetRailAvailability;
    private GetSearchPopularStationResponse responseGetSearchPopularStation;
}
