package org.example.data;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("org.example.data.RailData")
@Data
public class RailData {
    private String deptCity;
    private String deptStation;
    private String destCity;
    private String destStation;
    private String dateDeparture;
    private String dateReturn;
    private int totalAdultPassenger;
    private int totalInfantPassenger;
    private String timeDeparture;
    private String timeReturn;
    private String classDeparture;
    private String classReturn;
    private String maxPriceDeparture;
    private String minPriceDeparture;
    private String trainTypeDeparture;
    private String trainTypeReturn;
    private String customerTitle;
    private String customerName;
    private String customerPhoneNumber;
    private String customerEmail;
    private List<String> carriageDeparture;
    private List<String> seatDeparture;
    private List<String> carriageReturn;
    private List<String> seatReturn;
}
