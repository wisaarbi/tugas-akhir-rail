package org.example.data;

import io.restassured.http.Cookies;
import lombok.Data;
import org.example.responses.login.getuserlogin.GetUserLoginResponse;
import org.springframework.stereotype.Component;

@Component("org.example.data.LoginAPIData")
@Data
public class LoginAPIData {
    private Cookies loginCookies;

    private GetUserLoginResponse responseGetUserLogin;
}
