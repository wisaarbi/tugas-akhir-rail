package org.example.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.example.steps.BaseSteps;

import net.thucydides.core.steps.ScenarioSteps;

public class CucumberHooks extends ScenarioSteps implements BaseSteps {

    @Before
    public void beforeExecution() {

    }

    @After
    public void afterExecution() {

    }
}
