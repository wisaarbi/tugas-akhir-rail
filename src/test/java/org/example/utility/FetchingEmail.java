package org.example.utility;

import com.sun.mail.util.MailSSLSocketFactory;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

@Component("org.example.utility.FetchingEmail")
public class FetchingEmail {

    @SneakyThrows
    public String getVerificationCode(String host, String port, String storeType, String user, String password) {
        String verificationCode = "";
        MailSSLSocketFactory socketFactory = new MailSSLSocketFactory();
        socketFactory.setTrustAllHosts(true);

        Properties properties = new Properties();
        properties.setProperty("mail.store.protocol", storeType);
        properties.setProperty("mail.debug", "true");
        properties.setProperty("mail.imap.host", host);
        properties.setProperty("mail.imap.port", port);
        properties.setProperty("mail.imap.ssl.enable", "true");
        properties.put("mail.imap.ssl.socketFactory", socketFactory);
        Session emailSession = Session.getDefaultInstance(properties);

        Store store = emailSession.getStore(storeType);

        store.connect(host, user, password);

        Folder emailFolder = store.getFolder("INBOX");
        emailFolder.open(Folder.READ_ONLY);

        Message[] messages = emailFolder.getMessages();
        System.out.println("messages.length---" + messages.length);

        for (int i = messages.length - 1; i >= 0; i--) {
            Message message = messages[i];
            System.out.println("---------------------------------");
            if (message.getSubject().equals("Verifikasi untuk masuk akun Blibli dari perangkat baru")) {
                Object o = message.getContent();
                if (o instanceof String) {
                    String content = (String) o;
                    verificationCode = StringUtils.substringBetween(content, "\"font-size:24px;\">", "</strong>");
                }
                writePart(message);
                break;
            }
        }

        emailFolder.close(false);
        store.close();
        return verificationCode;
    }

    public void writePart(Part p) throws Exception {
        if (p instanceof Message) {
            writeEnvelope((Message) p);
        }

        System.out.println("----------------------------");
        System.out.println("CONTENT-TYPE: " + p.getContentType());

        if (p.isMimeType("text/plain")) {
            System.out.println("This is plain text");
            System.out.println("---------------------------");
            System.out.println((String) p.getContent());
        }
        else if (p.isMimeType("multipart/*")) {
            System.out.println("This is a Multipart");
            System.out.println("---------------------------");
            Multipart mp = (Multipart) p.getContent();
            int count = mp.getCount();
            for (int i = 0; i < count; i++)
                writePart(mp.getBodyPart(i));
        }
        else if (p.isMimeType("message/rfc822")) {
            System.out.println("This is a Nested Message");
            System.out.println("---------------------------");
            writePart((Part) p.getContent());
        }
        else if (p.getContentType().contains("image/")) {
            System.out.println("content type" + p.getContentType());
            File f = new File("image" + new Date().getTime() + ".jpg");
            DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
            com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = test.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } else {
            Object o = p.getContent();
            if (o instanceof String) {
                System.out.println("This is a string");
                System.out.println("---------------------------");
                System.out.println((String) o);
            } else if (o instanceof InputStream) {
                System.out.println("This is just an input stream");
                System.out.println("---------------------------");
                InputStream is = (InputStream) o;
                is = (InputStream) o;
                int c;
                while ((c = is.read()) != -1) {
                    System.out.write(c);
                }
            } else {
                System.out.println("This is an unknown type");
                System.out.println("---------------------------");
                System.out.println(o.toString());
            }
        }
    }

    public void writeEnvelope(Message m) throws Exception {
        System.out.println("This is the message envelope");
        System.out.println("---------------------------");
        Address[] a;

        if ((a = m.getFrom()) != null) {
            for (int j = 0; j < a.length; j++)
                System.out.println("FROM: " + a[j].toString());
        }

        if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
            for (int j = 0; j < a.length; j++)
                System.out.println("TO: " + a[j].toString());
        }

        if (m.getSubject() != null)
            System.out.println("SUBJECT: " + m.getSubject());

    }

}