package org.example.utility;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component("org.example.utility.RandomUtility")
public class RandomUtility {

    public String getSingleRandomItemFromList(List<String> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }

    public String generateRandomDigitNumber(int length) {
        Random rnd = new Random();
        double lower = Math.pow(10, length - 1);
        double upper = 9 * Math.pow(10, length - 1);
        double number = lower + rnd.nextInt((int) upper);
        return String.format("%.0f", number);
    }

}
