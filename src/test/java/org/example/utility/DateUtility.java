package org.example.utility;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component("org.example.utility.DateUtility")
public class DateUtility {

    public Date getCurrentDateWithDayPlus(int dayPlus) {
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, dayPlus);
        return c.getTime();
    }

    public String convertDateToString(Date date, String dateFormat) {
        DateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(date);
    }
}
