package org.example.responses.railavailability.getsearchpopularstation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class StationListItem{
	private String stationCompleteName;
	private String stationCode;
	private String cityName;
	private Object cityCode;
	private double latitude;
	private String stationName;
	private double longitude;
}
