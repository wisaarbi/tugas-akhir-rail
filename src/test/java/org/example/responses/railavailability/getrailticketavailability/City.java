package org.example.responses.railavailability.getrailticketavailability;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class City{
	private String cityName;
	private String cityCode;
	private double timeZone;
	private boolean popular;
	private int precedence;
}