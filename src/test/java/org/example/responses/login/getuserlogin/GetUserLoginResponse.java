package org.example.responses.login.getuserlogin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetUserLoginResponse {
	private int code;
	private UserLoginData data;
	private String status;
}