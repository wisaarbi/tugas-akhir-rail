package org.example.responses.checkholiday.getcheckholiday;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetCheckHolidayResponse{
	private String code;
	private long serverTime;
	private String message;
	private Object errors;
}
